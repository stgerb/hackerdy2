package ch.dxc.hackerdy.gamemode;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityGameModeBinding;
import ch.dxc.hackerdy.questionnaire.QuestionnaireActivity;

public class GameModeActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityGameModeBinding mBinding;
    private GameModeViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_game_mode);
        mViewModel = ViewModelProviders.of(this).get(GameModeViewModel.class);

        mBinding.buttonBattleMode.setOnClickListener(this);
        mBinding.buttonSinglePlayerMode.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewModel.getPhoto().observe(this, new Observer<Uri>()
        {
            @Override
            public void onChanged(Uri uri)
            {
                Picasso.get().load(uri).into(mBinding.imageAvatarSinglePlayer, new Callback()
                {
                    @Override
                    public void onSuccess()
                    {
                        mViewModel.setLoadingAvatarSinglePlayer(false);
                    }

                    @Override
                    public void onError(Exception e)
                    {
                    }
                });
                Picasso.get().load(uri).into(mBinding.imageAvatarBattleMode, new Callback()
                {
                    @Override
                    public void onSuccess()
                    {
                        mViewModel.setLoadingAvatarBattleMode(false);
                    }

                    @Override
                    public void onError(Exception e)
                    {
                    }
                });
            }
        });

        mViewModel.isLoadingAvatarSinglePlayer().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarAvatarSinglePlayer.bringToFront();
                    mBinding.progressbarAvatarSinglePlayer.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarAvatarSinglePlayer.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingAvatarBattleMode().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarAvatarBattleMode.bringToFront();
                    mBinding.progressbarAvatarBattleMode.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarAvatarBattleMode.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_single_player_mode:
                startActivity(new Intent(this, QuestionnaireActivity.class));
                break;
            case R.id.button_battle_mode:
                Snackbar.make(mBinding.activityGameMode, "Under construction...", Snackbar.LENGTH_LONG).show();
                break;
        }
    }
}