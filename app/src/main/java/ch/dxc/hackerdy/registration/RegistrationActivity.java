package ch.dxc.hackerdy.registration;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityRegistrationBinding;
import ch.dxc.hackerdy.util.PasswordUtil;
import ch.dxc.hackerdy.util.Regex;
import id.zelory.compressor.Compressor;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final int RC_PHOTO = 1;

    private static final String PHOTO = "photo";
    private static final String DEFAULT = "default";

    private ActivityRegistrationBinding mBinding;
    private RegistrationViewModel mViewModel;
    private PasswordUtil passwordUtil = new PasswordUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_registration);
        mViewModel = ViewModelProviders.of(this).get(RegistrationViewModel.class);
        mBinding.setViewModel(mViewModel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding.buttonCreateAccount.setOnClickListener(this);
        mBinding.imagePhoto.setOnClickListener(this);
        mBinding.textOptionalUndo.setOnClickListener(this);

        mBinding.inputUsername.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.layoutInputUsername.isErrorEnabled())
                {
                    mBinding.layoutInputUsername.setErrorEnabled(false);
                    mBinding.layoutInputUsername.setCounterEnabled(true);
                }
            }
        });

        mBinding.inputEmail.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.inputLayoutEmail.isErrorEnabled())
                {
                    mBinding.inputLayoutEmail.setErrorEnabled(false);
                }
            }
        });

        mBinding.inputPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.layoutInputPassword.isErrorEnabled())
                {
                    mBinding.layoutInputPassword.setErrorEnabled(false);
                }

                passwordUtil.showPasswordStrengthMeter(mBinding.textPasswordStrength, mBinding.progressbarPasswordStrength, editable);
                mBinding.textPasswordStrength.setText(getString(passwordUtil.getStringId()));
                mBinding.progressbarPasswordStrength.setProgress(passwordUtil.getProgress());
            }
        });

        mBinding.inputConfirmPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (mBinding.layoutInputConfirmPassword.isErrorEnabled())
                {
                    mBinding.layoutInputConfirmPassword.setErrorEnabled(false);
                }
            }
        });

        mViewModel.getUsernameError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.layoutInputUsername.setError(getString(resId));
                mBinding.layoutInputUsername.setCounterEnabled(false);
            }
        });

        mViewModel.getEmailError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.inputLayoutEmail.setError(getString(resId));
            }
        });

        mViewModel.getPasswordError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.layoutInputPassword.setError(getString(resId));

                mBinding.progressbarPasswordStrength.setVisibility(View.GONE);
                mBinding.textPasswordStrength.setVisibility(View.GONE);
            }
        });

        mViewModel.getConfirmPasswordError().observe(this, new Observer<Integer>()
        {
            @Override
            public void onChanged(Integer resId)
            {
                mBinding.layoutInputConfirmPassword.setError(getString(resId));
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbar.bringToFront();
                    mBinding.progressbar.setVisibility(View.VISIBLE);
                    mBinding.inputUsername.setEnabled(false);
                    mBinding.inputEmail.setEnabled(false);
                    mBinding.inputPassword.setEnabled(false);
                    mBinding.buttonCreateAccount.setEnabled(false);
                    mBinding.imagePhoto.setEnabled(false);
                    mBinding.textOptionalUndo.setEnabled(false);
                }
                else
                {
                    mBinding.progressbar.setVisibility(View.GONE);
                    mBinding.inputUsername.setEnabled(true);
                    mBinding.inputEmail.setEnabled(true);
                    mBinding.inputPassword.setEnabled(true);
                    mBinding.buttonCreateAccount.setEnabled(true);
                    mBinding.imagePhoto.setEnabled(true);
                    mBinding.textOptionalUndo.setEnabled(true);
                }
            }
        });

        mViewModel.getRegistrationSuccessful().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean registrationSuccessful)
            {
                Intent data = new Intent();
                data.putExtra("email", mBinding.inputEmail.getText().toString());
                data.putExtra("password", mBinding.inputPassword.getText().toString());
                setResult(Activity.RESULT_OK, data);
                finish();
            }
        });

        mViewModel.getRepositoryError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String msg)
            {
                Snackbar.make(mBinding.activityRegistration, msg, Snackbar.LENGTH_LONG).show();
            }
        });

        mViewModel.getPhoto().observe(this, new Observer<Bitmap>()
        {
            @Override
            public void onChanged(Bitmap photo)
            {
                mBinding.imagePhoto.setImageBitmap(photo);
                mBinding.imagePhoto.setTag(PHOTO);
                mBinding.textOptionalUndo.setText(R.string.text_undo);
            }
        });

        mViewModel.getPhotoError().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String msg)
            {
                Snackbar.make(mBinding.activityRegistration, msg, Snackbar.LENGTH_LONG).show();
            }
        });

        mViewModel.getUndo().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String text)
            {
                if (text.equals(getString(R.string.text_undo)))
                {
                    mBinding.textOptionalUndo.setText(R.string.text_undo);
                }
                else
                {
                    mBinding.textOptionalUndo.setText(text);
                }
            }
        });
    }

    private void measurePasswordStrength(String password)
    {
        if (password.matches(Regex.ONE_UPPER_NO_NUMERIC_NO_SPECIAL.getRegex()))
        {
            updatePasswordStrengthIndicator(R.string.text_weak, 38);
        }
        else if (password.matches(Regex.ONE_NUMERIC_NO_UPPER_NO_SPECIAL.getRegex()))
        {
            updatePasswordStrengthIndicator(R.string.text_weak, 38);
        }
        else if (password.matches(Regex.ONE_SPECIAL_NO_UPPER_NO_NUMERIC.getRegex()))
        {
            updatePasswordStrengthIndicator(R.string.text_weak, 38);
        }
        else if (password.matches(Regex.ONE_UPPER_ONE_NUMERIC.getRegex()))
        {
            updatePasswordStrengthIndicator(R.string.text_medium, 50);
        }
        else if (password.matches(Regex.ONE_UPPER_ONE_SPECIAL_NO_NUMERIC.getRegex()))
        {
            updatePasswordStrengthIndicator(R.string.text_medium, 50);
        }
        else if (password.matches(Regex.ONE_NUMERIC_ONE_SPECIAL_NO_UPPER.getRegex()))
        {
            updatePasswordStrengthIndicator(R.string.text_medium, 50);
        }
        else
        {
            updatePasswordStrengthIndicator(R.string.text_weak, 25);
        }
    }

    private void updatePasswordStrengthIndicator(int stringId, int progress)
    {
        mBinding.textPasswordStrength.setText(getString(stringId));
        mBinding.progressbarPasswordStrength.setProgress(progress);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_PHOTO)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                Compressor compressor = new id.zelory.compressor.Compressor(this);
                mViewModel.updatePhoto(data, compressor);
            }
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_create_account:
                mViewModel.createUserWithEmailAndPassword();
                break;
            case R.id.image_photo:
                Intent gallery = new Intent();
                gallery.setType("image/*");
                gallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(gallery, RC_PHOTO);
                break;
            case R.id.text_optional_undo:
                mBinding.imagePhoto.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.portrait));
                mBinding.textOptionalUndo.setText(R.string.text_optional);
                mBinding.imagePhoto.setTag(DEFAULT);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        BitmapDrawable photo = (BitmapDrawable) mBinding.imagePhoto.getDrawable();
        mViewModel.setPhoto(photo.getBitmap());
        mViewModel.setUndo(mBinding.textOptionalUndo.getText().toString());
    }
}