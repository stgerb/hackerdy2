package ch.dxc.hackerdy.registration;

public interface RegistrationCallback
{
    void onStart();

    void onSuccess();

    void onFailure(String msg);

    void usernameAlreadyExists();
}