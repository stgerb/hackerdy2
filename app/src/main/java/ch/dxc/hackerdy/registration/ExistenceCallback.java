package ch.dxc.hackerdy.registration;

public interface ExistenceCallback
{
    void isExistent(boolean bool);
}
