package ch.dxc.hackerdy.feedback.bug;

public interface ReportBugCallback
{
    void onStart();

    void onSubmit();

    void onFailure(String errorMsg);

}
