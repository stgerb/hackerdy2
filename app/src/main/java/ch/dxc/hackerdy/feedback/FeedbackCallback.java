package ch.dxc.hackerdy.feedback;

public interface FeedbackCallback
{
    void onStart();

    void onSubmit();

    void onFailure(String errorMsg);
}