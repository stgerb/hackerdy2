package ch.dxc.hackerdy.feedback;

public interface FeedbackRepository
{
    void submitFeedback(float ovalSatis, String goalAchieved, String infoUseful, String suggestions, FeedbackCallback feedback);
}