package ch.dxc.hackerdy.questionnaire;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class TopicLiveData extends LiveData<List<TopicListItem>> implements EventListener<QuerySnapshot>
{
    private Query mQuery;
    private ListenerRegistration mListener;

    public TopicLiveData(Query query)
    {
        mQuery = query;
    }

    @Override
    protected void onActive()
    {
        mListener = mQuery.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot querySnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (!querySnapshot.isEmpty() && querySnapshot != null)
        {
            List<TopicListItem> topicList = new ArrayList<>();
            for (QueryDocumentSnapshot document : querySnapshot)
            {
                String topic = document.getString("name");
                topicList.add(new TopicListItem(topic));
            }
            setValue(topicList);
        }
        else if (e != null)
        {
            // TODO Error handling
        }
    }
}