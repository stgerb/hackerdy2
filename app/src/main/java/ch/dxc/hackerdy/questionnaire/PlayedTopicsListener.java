package ch.dxc.hackerdy.questionnaire;

import java.util.List;

public interface PlayedTopicsListener
{
    void returnPlayedTopics(List<String>list);
}