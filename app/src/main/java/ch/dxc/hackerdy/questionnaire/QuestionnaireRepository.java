package ch.dxc.hackerdy.questionnaire;

import androidx.lifecycle.LiveData;

import java.util.List;

public interface QuestionnaireRepository
{
    LiveData<List<TopicListItem>> getTopics();

    void getPlayedTopics(PlayedTopicsListener listener);
}