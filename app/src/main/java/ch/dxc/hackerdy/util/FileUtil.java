package ch.dxc.hackerdy.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

public class FileUtil {

    public File createTempFile(String fileName, String fileExtension) throws IOException {
        File tempFile = File.createTempFile(fileName, fileExtension);
        tempFile.deleteOnExit();
        return tempFile;
    }

    public void copyInputStreamToFile(File file, InputStream inputStream) throws IOException {
        FileUtils.copyInputStreamToFile(inputStream, file);
    }

    public String fileLengthToMb(long fileLength) {
        double result = (double) fileLength / 1000000;
        DecimalFormat decimalFormat = new DecimalFormat("#0.##");
        decimalFormat.format(result);
        return decimalFormat.format(result);
    }

    public byte[] fileToByteArray(File file) throws IOException {
        return FileUtils.readFileToByteArray(file);

    }
}