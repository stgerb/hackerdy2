package ch.dxc.hackerdy.login;

import android.util.Log;
import android.util.Patterns;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseUser;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class LoginViewModel extends ViewModel implements LoginCallback
{
    public final ObservableField<String> mEmail = new ObservableField<>("");
    public final ObservableField<String> mPassword = new ObservableField<>("");

    private MutableLiveData<Integer> mEmailError = new MutableLiveData<>();
    private MutableLiveData<Integer> mPasswordError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<FirebaseUser> mEmailVerified = new MutableLiveData<>();
    private MutableLiveData<FirebaseUser> mLoginSuccessful = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();

    private FirebaseRepository mRepository = new FirebaseRepository();

    void signInWithEmailAndPassword()
    {
        String email = mEmail.get().trim();
        String password = mPassword.get().trim();

        if (email.isEmpty())
        {
            mEmailError.setValue(R.string.error_empty_email);
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            mEmailError.setValue(R.string.error_email_format);
        }
        else if (password.isEmpty())
        {
            mPasswordError.setValue(R.string.error_empty_password);
        }
        else
        {
            mRepository.signInWithEmailAndPassword(email, password, this);
        }
    }

    void signInWithGoogle(GoogleSignInAccount account)
    {
        mRepository.signInWithGoogle(account, this);
    }

    void signInWithFacebook(AccessToken token)
    {
        mRepository.signInWithFacebook(token, this);
    }

    void autoLogin()
    {
        FirebaseUser user = mRepository.getUser();
        if (user != null && user.isEmailVerified())
        {
            mLoading.setValue(true);
            mRepository.updateLoginTimestamp(user, this);
        }
    }

    @Override
    public void onStart()
    {
        mLoading.setValue(true);
    }

    @Override
    public void onSuccess(FirebaseUser user)
    {
        mLoading.setValue(false);
        mLoginSuccessful.setValue(user);
    }

    @Override
    public void onFailure(String msg)
    {
        mLoading.setValue(false);
        mRepositoryError.setValue(msg);
    }

    @Override
    public void emailNotVerified(FirebaseUser user)
    {
        mLoading.setValue(false);
        mEmailVerified.setValue(user);
    }

    LiveData<Integer> getEmailError()
    {
        return mEmailError;
    }

    LiveData<Integer> getPasswordError()
    {
        return mPasswordError;
    }

    LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    LiveData<FirebaseUser> getLoginSuccessful()
    {
        return mLoginSuccessful;
    }

    LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    LiveData<FirebaseUser> getEmailVerified()
    {
        return mEmailVerified;
    }
}