package ch.dxc.hackerdy.login;

import com.google.firebase.auth.FirebaseUser;

public interface LoginCallback
{
    void onStart();

    void onSuccess(FirebaseUser user);

    void onFailure(String msg);

    void emailNotVerified(FirebaseUser user);
}