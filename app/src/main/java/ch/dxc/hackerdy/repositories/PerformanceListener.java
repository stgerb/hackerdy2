package ch.dxc.hackerdy.repositories;

import java.util.Map;

public interface PerformanceListener
{
    void getPerformanceData(Map<String, Object> data);
}