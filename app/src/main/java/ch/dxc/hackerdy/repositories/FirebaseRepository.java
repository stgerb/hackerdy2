package ch.dxc.hackerdy.repositories;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ch.dxc.hackerdy.feedback.FeedbackCallback;
import ch.dxc.hackerdy.feedback.FeedbackRepository;
import ch.dxc.hackerdy.feedback.bug.ReportBugCallback;
import ch.dxc.hackerdy.feedback.bug.ReportBugRepository;
import ch.dxc.hackerdy.forgotpassword.ForgotPasswordRepository;
import ch.dxc.hackerdy.forgotpassword.PasswordResetCallback;
import ch.dxc.hackerdy.highscores.HighScoresListItem;
import ch.dxc.hackerdy.highscores.HighScoresLiveData;
import ch.dxc.hackerdy.highscores.HighScoresRepository;
import ch.dxc.hackerdy.home.HomeRepository;
import ch.dxc.hackerdy.login.LoginCallback;
import ch.dxc.hackerdy.login.LoginRepository;
import ch.dxc.hackerdy.questionnaire.PlayedTopicsListener;
import ch.dxc.hackerdy.questionnaire.QuestionnaireRepository;
import ch.dxc.hackerdy.questionnaire.TopicListItem;
import ch.dxc.hackerdy.questionnaire.TopicLiveData;
import ch.dxc.hackerdy.quiz.question.Question;
import ch.dxc.hackerdy.quiz.question.QuestionLiveData;
import ch.dxc.hackerdy.quiz.question.QuestionRepository;
import ch.dxc.hackerdy.quiz.result.LevelDataLiveData;
import ch.dxc.hackerdy.quiz.result.ProgressStatsLiveData;
import ch.dxc.hackerdy.quiz.result.ResultCallback;
import ch.dxc.hackerdy.quiz.result.ResultRepository;
import ch.dxc.hackerdy.registration.ExistenceCallback;
import ch.dxc.hackerdy.registration.RegistrationCallback;
import ch.dxc.hackerdy.registration.RegistrationRepository;
import ch.dxc.hackerdy.registration.UploadCallback;
import ch.dxc.hackerdy.settings.UpdateUserAccountDataCallback;
import ch.dxc.hackerdy.settings.UpdateUserAccountDataRepository;
import ch.dxc.hackerdy.stats.PerformanceStatsLiveData;
import ch.dxc.hackerdy.stats.StatsRepository;
import ch.dxc.hackerdy.stats.TopicStatsLiveData;
import ch.dxc.hackerdy.stats.UserDataLiveData;

public class FirebaseRepository implements LoginRepository, RegistrationRepository, ForgotPasswordRepository, HomeRepository, QuestionnaireRepository, QuestionRepository, ResultRepository, StatsRepository, HighScoresRepository, FeedbackRepository, ReportBugRepository, UpdateUserAccountDataRepository
{
    private static final String COLLECTION_USERS = "users";
    private static final String COLLECTION_QUESTIONS = "questions";
    private static final String COLLECTION_TOPICS = "topics";
    private static final String COLLECTION_LEVELS = "levels";
    private static final String COLLECTION_STATS = "stats";
    private static final String COLLECTION_TROPHIES = "trophies";
    private static final String COLLECTION_FEEDBACK = "feedback";
    private static final String COLLECTION_BUGS = "bugs";

    private static final String DOCUMENT_PROGRESS = "progress";
    private static final String DOCUMENT_PERFORMANCE = "performance";
    private static final String DOCUMENT_FACTS_NUMS = "Facts and numbers";
    private static final String DOCUMENT_IT_SEC_OVERALL = "IT Security overall";
    private static final String DOCUMENT_IT_SEC_ROAD = "IT Security on the road";
    private static final String DOCUMENT_IT_SEC_OFFICE = "IT Security at the office";
    private static final String DOCUMENT_IAM = "Identity and Access Management";
    private static final String DOCUMENT_IT_SEC_DIGITAL = "IT Security in the digital world";
    private static final String DOCUMENT_IT_SEC_DATA_DOC = "IT Security for data and documents";
    private static final String DOCUMENT_IT_SEC_USELESS = "Useless knowledge about IT Security";
    private static final String DOCUMENT_IT_SEC_HARD_SOFT = "IT Security for hardware and software";

    private static final String FIELD_PLAY_COUNT = "play_count";
    private static final String FIELD_TOPIC_PLAYED = "topic_played";
    private static final String FIELD_DATE_JOINED = "date_joined";
    private static final String FIELD_DATE_LAST_LOGIN = "date_last_login";
    private static final String FIELD_USERNAME = "username";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_PHOTO = "photo";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_TOPIC = "topic";
    private static final String FIELD_PLAYED_TOPICS = "played_topics";
    private static final String FIELD_POINTS = "points";
    private static final String FIELD_QUESTION = "question";
    private static final String FIELD_ANSWERS = "answers";
    private static final String FIELD_SOLUTION = "solution";
    private static final String FIELD_REASON = "reason";
    private static final String FIELD_LEVEL = "level";
    private static final String FIELD_BALANCE = "balance";
    private static final String FIELD_ANSWERED_CORRECT = "answered_correct";
    private static final String FIELD_ANSWERED_WRONG = "answered_wrong";
    private static final String FIELD_PERSONAL_BEST = "personal_best";
    private static final String FIELD_PLAYED_QUIZZES = "played_quizzes";
    private static final String FIELD_PLAY_TIME = "play_time";
    private static final String FIELD_SUBMITTED_FEEDBACK = "submitted_feedback";
    private static final String FIELD_REPORTED_ON = "reported_on";
    private static final String FIELD_SUBMITTED_ON = "submitted_on";
    private static final String FIELD_USER = "user";
    private static final String FIELD_OVAL_SATISFACTION = "oval_satisfaction";
    private static final String FIELD_GOAL_ACHIEVED = "goal_achieved";
    private static final String FIELD_INFO_USEFUL = "info_useful";
    private static final String FIELD_SUGGESTIONS = "suggestions";
    private static final String FIELD_BUG_TITLE = "bug_title";
    private static final String FIELD_BUG_DESCRIPTION = "bug_description";

    private static final String PROVIDER_ID_GOOGLE = "google.com";
    private static final String PROVIDER_ID_FACEBOOK = "facebook.com";
    private static final String PROVIDER_ID_FIREBASE = "firebase";

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd hh:mm:ss";
    private static final int RESET_PLAYED_TOPICS_COUNT = 9;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore mDatabase = FirebaseFirestore.getInstance();
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();
    private LoginManager mLoginManager = LoginManager.getInstance();

    @Override
    public void signInWithEmailAndPassword(String email,
                                           String password,
                                           final LoginCallback login)
    {
        login.onStart();

        mAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>()
        {
            @Override
            public void onSuccess(AuthResult authResult)
            {
                FirebaseUser user = getUser();

                if (user.isEmailVerified())
                {
                    updateLoginTimestamp(user, login);
                }
                else
                {
                    login.emailNotVerified(user);
                }
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                login.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void updateLoginTimestamp(final FirebaseUser user,
                                     final LoginCallback login)
    {
        Map<String, Object> data = new HashMap<>();
        data.put(FIELD_DATE_LAST_LOGIN, new SimpleDateFormat(DATE_TIME_PATTERN, Locale.getDefault()).format(new Date()));

        mDatabase.collection(COLLECTION_USERS).document(user.getUid()).update(data).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                login.onSuccess(user);
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                mAuth.signOut();
                login.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public FirebaseUser getUser()
    {
        return mAuth.getCurrentUser();
    }

    @Override
    public void signInWithGoogle(GoogleSignInAccount account,
                                 final LoginCallback login)
    {
        login.onStart();

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnSuccessListener(new OnSuccessListener<AuthResult>()
        {
            @Override
            public void onSuccess(AuthResult authResult)
            {
                FirebaseUser user = getUser();
                boolean isNewUser = authResult.getAdditionalUserInfo().isNewUser();

                if (isNewUser)
                {
                    persistUser(user, login);
                }
                else
                {
                    updateLoginTimestamp(user, login);
                }
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                login.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void signInWithFacebook(AccessToken token,
                                   final LoginCallback login)
    {
        login.onStart();

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnSuccessListener(new OnSuccessListener<AuthResult>()
        {
            @Override
            public void onSuccess(AuthResult authResult)
            {
                FirebaseUser user = getUser();
                boolean isNewUser = authResult.getAdditionalUserInfo().isNewUser();

                if (isNewUser)
                {
                    persistUser(user, login);
                }
                else
                {
                    updateLoginTimestamp(user, login);
                }
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                login.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void persistUser(final FirebaseUser user,
                            final LoginCallback login)
    {
        final WriteBatch batch = mDatabase.batch();

        Map<String, Object> userMap = new HashMap<>();
        userMap.put(FIELD_DATE_JOINED, new SimpleDateFormat(DATE_TIME_PATTERN, Locale.getDefault()).format(new Date()));
        userMap.put(FIELD_USERNAME, getUser().getDisplayName());
        userMap.put(FIELD_EMAIL, getUser().getEmail());
        userMap.put(FIELD_PHOTO, getUser().getPhotoUrl().toString());
        userMap.put(FIELD_SUBMITTED_FEEDBACK, false);

        Map<String, Object> progressMap = new HashMap<>();
        progressMap.put(FIELD_BALANCE, 0);
        progressMap.put(FIELD_LEVEL, 1);

        Map<String, Object> performanceMap = new HashMap<>();
        performanceMap.put(FIELD_ANSWERED_CORRECT, 0);
        performanceMap.put(FIELD_ANSWERED_WRONG, 0);
        performanceMap.put(FIELD_PERSONAL_BEST, new HashMap<>());
        performanceMap.put(FIELD_PLAY_TIME, "00:00:00");
        performanceMap.put(FIELD_PLAYED_QUIZZES, 0);

        final Map<String, Object> topicStatsMap = new HashMap<>();
        topicStatsMap.put(FIELD_ANSWERED_CORRECT, 0);
        topicStatsMap.put(FIELD_ANSWERED_WRONG, 0);
        topicStatsMap.put(FIELD_TOPIC_PLAYED, false);
        topicStatsMap.put(FIELD_PLAY_COUNT, 0);

        DocumentReference userRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid());
        batch.set(userRef, userMap);

        DocumentReference progressRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PROGRESS);
        batch.set(progressRef, progressMap);

        DocumentReference performanceRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PERFORMANCE);
        batch.set(performanceRef, performanceMap);

        DocumentReference trophiesRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_TROPHIES).document();
        batch.set(trophiesRef, new HashMap<>());

        getTopics(new TopicsListener()
        {
            @Override
            public void get(List<String> topicList)
            {
                if (topicList != null)
                {
                    for (String topic : topicList)
                    {
                        DocumentReference topicStatsRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_STATS).document(topic);
                        batch.set(topicStatsRef, topicStatsMap);
                    }

                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            updateLoginTimestamp(user, login);
                        }
                    }).addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            user.delete();
                            login.onFailure(e.getMessage());
                        }
                    });
                }
                else
                {
                    // TODO add error Message! R.string.
                    login.onFailure(null);
                }
            }
        });
    }

    @Override
    public void createUserWithEmailAndPassword(final String username,
                                               final String email,
                                               final String password,
                                               final byte[] photo,
                                               final RegistrationCallback registration)
    {
        registration.onStart();

        checkUserDataExistence(username, FIELD_USERNAME, new ExistenceCallback()
        {
            @Override
            public void isExistent(boolean exists)
            {
                if (exists)
                {
                    registration.usernameAlreadyExists();
                }
                else
                {
                    mAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>()
                    {
                        @Override
                        public void onSuccess(AuthResult authResult)
                        {
                            final FirebaseUser user = getUser();

                            user.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(username).build());
                            uploadPhoto(user, photo, new UploadCallback()
                            {
                                @Override
                                public void onSuccess(FirebaseUser user, String photoUrl)
                                {
                                    persistUser(user, photoUrl, registration);
                                }

                                @Override
                                public void onFailure(String msg)
                                {
                                    user.delete();
                                    registration.onFailure(msg);
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            registration.onFailure(e.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void persistUser(final FirebaseUser user,
                            String photoUrl,
                            final RegistrationCallback registration)
    {
        final WriteBatch batch = mDatabase.batch();

        Map<String, Object> userMap = new HashMap<>();
        userMap.put(FIELD_DATE_JOINED, new SimpleDateFormat(DATE_TIME_PATTERN, Locale.getDefault()).format(new Date()));
        userMap.put(FIELD_USERNAME, user.getDisplayName());
        userMap.put(FIELD_EMAIL, user.getEmail());
        userMap.put(FIELD_PHOTO, photoUrl);
        userMap.put(FIELD_SUBMITTED_FEEDBACK, false);

        Map<String, Object> progressMap = new HashMap<>();
        progressMap.put(FIELD_BALANCE, 0);
        progressMap.put(FIELD_LEVEL, 1);

        Map<String, Object> performanceMap = new HashMap<>();
        performanceMap.put(FIELD_ANSWERED_CORRECT, 0);
        performanceMap.put(FIELD_ANSWERED_WRONG, 0);
        performanceMap.put(FIELD_PERSONAL_BEST, new HashMap<>());
        performanceMap.put(FIELD_PLAY_TIME, "00:00:00");
        performanceMap.put(FIELD_PLAYED_QUIZZES, 0);

        final Map<String, Object> topicStatsMap = new HashMap<>();
        topicStatsMap.put(FIELD_ANSWERED_CORRECT, 0);
        topicStatsMap.put(FIELD_ANSWERED_WRONG, 0);
        topicStatsMap.put(FIELD_TOPIC_PLAYED, false);
        topicStatsMap.put(FIELD_PLAY_COUNT, 0);

        DocumentReference userRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid());
        batch.set(userRef, userMap);

        DocumentReference progressRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PROGRESS);
        batch.set(progressRef, progressMap);

        DocumentReference performanceRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PERFORMANCE);
        batch.set(performanceRef, performanceMap);

        DocumentReference trophiesRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_TROPHIES).document();
        batch.set(trophiesRef, new HashMap<>());

        getTopics(new TopicsListener()
        {
            @Override
            public void get(List<String> topicList)
            {
                if (topicList != null)
                {
                    for (String topic : topicList)
                    {
                        DocumentReference topicStatsRef = mDatabase.collection(COLLECTION_USERS).document(user.getUid()).collection(COLLECTION_STATS).document(topic);
                        batch.set(topicStatsRef, topicStatsMap);
                    }

                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            user.sendEmailVerification();
                            mAuth.signOut();

                            registration.onSuccess();
                        }
                    }).addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            user.delete();
                            registration.onFailure(e.getMessage());
                        }
                    });
                }
                else
                {
                    // TODO add error Message! R.string.
                    registration.onFailure(null);
                }
            }
        });
    }

    @Override
    public void uploadPhoto(final FirebaseUser user,
                            byte[] photo,
                            final UploadCallback uploadCallback)
    {
        StorageReference storageReference = mStorage.getReference(String.format("imgs/users/%s/profile/photo.png", user.getUid()));
        UploadTask uploadTask = storageReference.putBytes(photo);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
        {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
            {
                Task<Uri> url = taskSnapshot.getStorage().getDownloadUrl();
                while (!url.isComplete()) ;
                Uri uri = url.getResult();

                user.updateProfile(new UserProfileChangeRequest.Builder().setPhotoUri(uri).build());

                uploadCallback.onSuccess(user, uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                uploadCallback.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void sendPasswordReset(String email, final PasswordResetCallback reset)
    {
        reset.onStart();

        mAuth.sendPasswordResetEmail(email).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                reset.onSuccess();
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                reset.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void signOut(GoogleSignInClient googleClient)
    {
        List<String> providerList = getProviderData();

        for (String id : providerList)
        {
            switch (id)
            {
                case PROVIDER_ID_GOOGLE:
                    googleClient.signOut();
                    break;
                case PROVIDER_ID_FIREBASE:
                    mAuth.signOut();
                    break;
                case PROVIDER_ID_FACEBOOK:
                    mLoginManager.logOut();
                    break;
            }
        }
    }

    @Override
    public LiveData<List<TopicListItem>> getTopics()
    {
        Query query = mDatabase.collection(COLLECTION_TOPICS);
        return new TopicLiveData(query);
    }

    @Override
    public LiveData<Question> getQuestion(String topic, long points)
    {
        Query query = mDatabase.collection(COLLECTION_QUESTIONS).whereEqualTo(FIELD_TOPIC, topic).whereEqualTo(FIELD_POINTS, points);
        return new QuestionLiveData(query);
    }

    @Override
    public LiveData<Map<String, Object>> getProgressStats(String id)
    {
        DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(id).collection(COLLECTION_STATS).document(DOCUMENT_PROGRESS);

        return new ProgressStatsLiveData(ref);
    }

    @Override
    public LiveData<Map<String, Long>> getLevelData(long level)
    {
        Query query = mDatabase.collection(COLLECTION_LEVELS).whereEqualTo(FIELD_LEVEL, level);
        return new LevelDataLiveData(query);
    }

    @Override
    public void updateStats(final long score,
                            final List<String> playedTopicsList,
                            Map<String, Integer> answeredCorrectlyWrongStats,
                            Map<String, Integer> topic1Stats,
                            Map<String, Integer> topic2Stats,
                            Map<String, Integer> topic3Stats,
                            final String time,
                            final ResultCallback repo)
    {
        final WriteBatch batch = mDatabase.batch();

        // Update balance
        if (score > 0)
        {
            DocumentReference balanceRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PROGRESS);
            batch.update(balanceRef, FIELD_BALANCE, FieldValue.increment(score));
        }

        // Update played topics
        for (String topic : playedTopicsList)
        {
            DocumentReference playedTopicsRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(topic);
            batch.update(playedTopicsRef, FIELD_TOPIC_PLAYED, true);
        }

        // Update topic stats
        DocumentReference topic1StatsRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(playedTopicsList.get(0));
        batch.update(topic1StatsRef, FIELD_ANSWERED_CORRECT, FieldValue.increment(topic1Stats.get("answered_correct")));
        batch.update(topic1StatsRef, FIELD_ANSWERED_WRONG, FieldValue.increment(topic1Stats.get("answered_wrong")));
        batch.update(topic1StatsRef, FIELD_PLAY_COUNT, FieldValue.increment(1));

        DocumentReference topic2StatsRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(playedTopicsList.get(1));
        batch.update(topic2StatsRef, FIELD_ANSWERED_CORRECT, FieldValue.increment(topic2Stats.get("answered_correct")));
        batch.update(topic2StatsRef, FIELD_ANSWERED_WRONG, FieldValue.increment(topic2Stats.get("answered_wrong")));
        batch.update(topic2StatsRef, FIELD_PLAY_COUNT, FieldValue.increment(1));

        DocumentReference topic3StatsRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(playedTopicsList.get(2));
        batch.update(topic3StatsRef, FIELD_ANSWERED_CORRECT, FieldValue.increment(topic3Stats.get("answered_correct")));
        batch.update(topic3StatsRef, FIELD_ANSWERED_WRONG, FieldValue.increment(topic3Stats.get("answered_wrong")));
        batch.update(topic3StatsRef, FIELD_PLAY_COUNT, FieldValue.increment(1));

        // Update performance
        final DocumentReference performanceRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PERFORMANCE);
        batch.update(performanceRef, FIELD_ANSWERED_CORRECT, FieldValue.increment(answeredCorrectlyWrongStats.get("answered_correct")));
        batch.update(performanceRef, FIELD_ANSWERED_WRONG, FieldValue.increment(answeredCorrectlyWrongStats.get("answered_wrong")));
        batch.update(performanceRef, FIELD_PLAYED_QUIZZES, FieldValue.increment(1));

        getPerformanceData(new PerformanceListener()
        {
            @Override
            public void getPerformanceData(Map<String, Object> data)
            {
                String playTime = (String) data.get("play_time");

                if (playTime != null)
                {
                    String[] playTimeArray = playTime.split(":");
                    int hours = Integer.parseInt(playTimeArray[0]);
                    int minutesPlayTime = Integer.parseInt(playTimeArray[1]);
                    int secondsPlayTime = Integer.parseInt(playTimeArray[2]);

                    String[] timeArray = time.split(":");
                    int minutesNeededTime = Integer.parseInt(timeArray[0]);
                    int secondsNeededTime = Integer.parseInt(timeArray[1]);

                    int minutes = minutesPlayTime + minutesNeededTime;
                    int seconds = secondsPlayTime + secondsNeededTime;

                    if (seconds >= 60)
                    {
                        minutes++;
                        seconds -= 60;
                    }

                    if (minutes >= 60)
                    {
                        hours++;
                        minutes -= 60;
                    }

                    StringBuilder newPlayTime = new StringBuilder();
                    if (hours >= 0 && hours < 10)
                    {
                        newPlayTime.append("0" + hours + ":");
                    }
                    else
                    {
                        newPlayTime.append(hours + ":");
                    }

                    if (minutes >= 0 && minutes < 10)
                    {
                        newPlayTime.append("0" + minutes + ":");
                    }
                    else
                    {
                        newPlayTime.append(minutes + ":");
                    }

                    if (seconds >= 0 && seconds < 10)
                    {
                        newPlayTime.append("0" + seconds);
                    }
                    else
                    {
                        newPlayTime.append(seconds);
                    }

                    batch.update(performanceRef, FIELD_PLAY_TIME, newPlayTime.toString());
                }

                batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        resetPlayedTopics(repo);
                    }
                }).addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        repo.onFailure(e.getMessage());
                    }
                });
            }
        });
    }

    @Override
    public void resetPlayedTopics(final ResultCallback repo)
    {
        Query query = queryPlayedTopics();
        query.addSnapshotListener(new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e)
            {
                if (queryDocumentSnapshots != null && queryDocumentSnapshots.size() == RESET_PLAYED_TOPICS_COUNT)
                {
                    List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();

                    WriteBatch batch = mDatabase.batch();

                    for (DocumentSnapshot snapshot : snapshotList)
                    {
                        DocumentReference ref = snapshot.getReference();
                        batch.update(ref, FIELD_TOPIC_PLAYED, false);
                    }

                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            // not in use
                        }
                    }).addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            repo.onFailure(e.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void updateLevel(final long level,
                            final ResultCallback repo,
                            final ResultCallback.ProgressStatsCallback update)
    {
        mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PROGRESS).update(FIELD_LEVEL, level).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                update.onLevelUp(level);
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                repo.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void updatePersonalBest(final long score, final String time, final ResultCallback.PerformanceStatsCallback performance, final ResultCallback repo)
    {
        final Map<String, Object> personalBestMap = new HashMap<>();
        personalBestMap.put("score", score);
        personalBestMap.put("time", time);
        mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PERFORMANCE).update(FIELD_PERSONAL_BEST, personalBestMap).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                performance.getPersonalBest(personalBestMap);
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                repo.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void getPerformanceData(final PerformanceListener callback)
    {
        mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).document(DOCUMENT_PERFORMANCE).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>()
        {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot)
            {
                Map<String, Object> data = new HashMap<>();
                data.put("answered_correct", documentSnapshot.getLong(FIELD_ANSWERED_CORRECT));
                data.put("answered_wrong", documentSnapshot.getLong(FIELD_ANSWERED_WRONG));
                data.put("personal_best", documentSnapshot.get(FIELD_PERSONAL_BEST));
                data.put("play_time", documentSnapshot.getString(FIELD_PLAY_TIME));
                data.put("played_quizzes", documentSnapshot.getLong(FIELD_PLAYED_QUIZZES));

                callback.getPerformanceData(data);
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                callback.getPerformanceData(null);
            }
        });
    }

    @Override
    public LiveData<Map<String, Object>> getUserData(String id)
    {
        DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(id);

        return new UserDataLiveData(ref);
    }

    @Override
    public LiveData<Map<String, Object>> getPerformanceStats(String id)
    {
        DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(id).collection(COLLECTION_STATS).document(DOCUMENT_PERFORMANCE);

        return new PerformanceStatsLiveData(ref);
    }

    @Override
    public LiveData<Map<String, Long>> getTopicStats(String topic, String id)
    {
        DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(id).collection(COLLECTION_STATS).document(topic);

        return new TopicStatsLiveData(ref);
    }

    @Override
    public LiveData<List<HighScoresListItem>> getHighScores()
    {
        Query query = mDatabase.collection(COLLECTION_USERS);

        return new HighScoresLiveData(query);
    }

    @Override
    public void changeUsername(final String username, final UpdateUserAccountDataCallback.UpdateCallback update, final UpdateUserAccountDataCallback.UsernameCallback callback)
    {
        update.onStart();

        checkUserDataExistence(username, FIELD_USERNAME, new ExistenceCallback()
        {
            @Override
            public void isExistent(boolean exists)
            {
                if (exists)
                {
                    callback.usernameAlreadyExists();
                }
                else
                {
                    FirebaseUser user = getUser();

                    // TODO Unsolved error scenario: When database update fails, previous email change in Firebase Auth must be reverted. if not, data will be inconsistent.
                    // Firebase Auth
                    user.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(username).build());

                    // Firebase Firestore
                    DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid());
                    WriteBatch batch = mDatabase.batch();
                    batch.update(ref, FIELD_USERNAME, username);
                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                    {
                        @Override
                        public void onSuccess(Void aVoid)
                        {
                            callback.onUsernameChangeSuccessful(username);
                        }
                    }).addOnFailureListener(new OnFailureListener()
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            update.onFailure(e.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void submitFeedback(float ovalSatis, String goalAchieved, String infoUseful, String suggestions, final FeedbackCallback feedback)
    {
        feedback.onStart();

        Map<String, Object> data = new HashMap<>();
        data.put(FIELD_SUBMITTED_ON, new SimpleDateFormat(DATE_TIME_PATTERN, Locale.getDefault()).format(new Date()));
        data.put(FIELD_USER, mDatabase.document(String.format("users/%s/", getUser().getUid())));
        data.put(FIELD_OVAL_SATISFACTION, ovalSatis);
        data.put(FIELD_GOAL_ACHIEVED, goalAchieved);
        data.put(FIELD_INFO_USEFUL, infoUseful);
        data.put(FIELD_SUGGESTIONS, suggestions);

        WriteBatch batch = mDatabase.batch();

        DocumentReference feedbackRef = mDatabase.collection(COLLECTION_FEEDBACK).document();
        batch.set(feedbackRef, data);

        DocumentReference usersRef = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid());
        batch.update(usersRef, FIELD_SUBMITTED_FEEDBACK, true);

        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                feedback.onSubmit();
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                feedback.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void submitBugReport(String description, final ReportBugCallback bug)
    {
        bug.onStart();

        HashMap<String, Object> data = new HashMap<>();
        data.put(FIELD_USER, mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()));
        data.put(FIELD_REPORTED_ON, new SimpleDateFormat(DATE_TIME_PATTERN, Locale.getDefault()).format(new Date()));
        data.put(FIELD_BUG_DESCRIPTION, description);

        WriteBatch batch = mDatabase.batch();

        DocumentReference ref = mDatabase.collection(COLLECTION_BUGS).document();
        batch.set(ref, data);

        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                bug.onSubmit();
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                bug.onFailure(e.getMessage());
            }
        });
    }

    @Override
    public void getPlayedTopics(final PlayedTopicsListener listener)
    {
        Query query = queryPlayedTopics();
        query.addSnapshotListener(new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e)
            {
                List<String> list = new ArrayList<>();

                for (DocumentSnapshot snapshot : queryDocumentSnapshots)
                {
                    list.add(snapshot.getId());
                }

                listener.returnPlayedTopics(list);
            }
        });
    }

    @Override
    public void changePassword(String password, final UpdateUserAccountDataCallback.UpdateCallback update, final UpdateUserAccountDataCallback.PasswordCallback callback)
    {
        update.onStart();

        getUser().updatePassword(password).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                callback.onPasswordChangeSuccessful();
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                if (e instanceof FirebaseAuthRecentLoginRequiredException)
                {
                    update.reauthenticate(e.getMessage());
                }
                else
                {
                    update.onFailure(e.getMessage());
                }
            }
        });
    }

    @Override
    public void changeEmail(final String email, final UpdateUserAccountDataCallback.UpdateCallback update, final UpdateUserAccountDataCallback.EmailCallback callback)
    {
        update.onStart();

        getUser().updateEmail(email).addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                WriteBatch batch = mDatabase.batch();
                DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(getUser().getUid());

                batch.update(ref, FIELD_EMAIL, email);

                batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        getUser().sendEmailVerification();
                        callback.onEmailChangeSuccessful(email);
                    }
                }).addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        // TODO Unsolved error scenario: When database update fails, previous email change in Firebase Auth must be reverted. if not, data will be inconsistent.
                        update.onFailure(e.getMessage());
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                if (e instanceof FirebaseAuthRecentLoginRequiredException)
                {
                    update.reauthenticate(e.getMessage());
                }
                else
                {
                    update.onFailure(e.getMessage());
                }
            }
        });
    }

    @Override
    public void deleteAccount(final UpdateUserAccountDataCallback.UpdateCallback update, final UpdateUserAccountDataCallback.DeleteAccountCallback delete)
    {
        update.onStart();

        final String id = getUser().getUid();

        getUser().delete().addOnSuccessListener(new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                final WriteBatch batch = mDatabase.batch();
                final DocumentReference ref = mDatabase.collection(COLLECTION_USERS).document(id);
                Task<QuerySnapshot> statsTask = mDatabase.collection(COLLECTION_USERS).document(id).collection(COLLECTION_STATS).get();
                final Task<QuerySnapshot> trophiesTask = mDatabase.collection(COLLECTION_USERS).document(id).collection(COLLECTION_TROPHIES).get();

                // TODO What if this fails? => previous changes must be reverted
                statsTask.addOnSuccessListener(new OnSuccessListener<QuerySnapshot>()
                {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                    {
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                        {
                            batch.delete(snapshot.getReference());
                        }

                        // TODO What if this fails? => previous changes must be reverted
                        trophiesTask.addOnSuccessListener(new OnSuccessListener<QuerySnapshot>()
                        {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                            {
                                for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                                {
                                    batch.delete(snapshot.getReference());
                                }

                                // TODO What if this fails? => previous changes must be reverted
                                StorageReference storageReference = mStorage.getReference();
                                StorageReference desertRef = storageReference.child(String.format("imgs/users/%s/profile/photo.png", id));
                                desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>()
                                {
                                    @Override
                                    public void onSuccess(Void aVoid)
                                    {
                                        batch.delete(ref);

                                        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>()
                                        {
                                            @Override
                                            public void onSuccess(Void aVoid)
                                            {
                                                delete.onAccountDeleted();
                                            }
                                        }).addOnFailureListener(new OnFailureListener()
                                        {
                                            @Override
                                            public void onFailure(@NonNull Exception e)
                                            {
                                                update.onFailure(e.getMessage());
                                            }
                                        });
                                    }
                                }).addOnFailureListener(new OnFailureListener()
                                {
                                    @Override
                                    public void onFailure(@NonNull Exception e)
                                    {
                                        update.onFailure(e.getMessage());
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener()
                        {
                            @Override
                            public void onFailure(@NonNull Exception e)
                            {
                                update.onFailure(e.getMessage());
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        update.onFailure(e.getMessage());
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                if (e instanceof FirebaseAuthRecentLoginRequiredException)
                {
                    update.reauthenticate(e.getMessage());
                }
                else
                {
                    update.onFailure(e.getMessage());
                }
            }
        });
    }

    @Override
    public Boolean isFederatedIdentityLogin()
    {
        List<String> providerData = getProviderData();

        if (providerData.contains(PROVIDER_ID_GOOGLE) || providerData.contains(PROVIDER_ID_FACEBOOK))
        {
            return true;
        }
        return false;
    }

    private void checkUserDataExistence(final String data, final String field, final ExistenceCallback callback)
    {
        Query query = mDatabase.collection(COLLECTION_USERS);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task)
            {
                if (task.isSuccessful())
                {
                    boolean exists = false;

                    for (DocumentSnapshot snapshot : task.getResult())
                    {
                        String result = snapshot.getString(field).toLowerCase();

                        if (result.equals(data.toLowerCase()))
                        {
                            exists = true;

                            break;
                        }
                    }

                    if (exists)
                    {
                        callback.isExistent(true);
                    }
                    else
                    {
                        callback.isExistent(false);
                    }
                }

                if (task.getResult().size() == 0)
                {
                    callback.isExistent(false);
                }
            }
        });
    }

    private Query queryPlayedTopics()
    {
        return mDatabase.collection(COLLECTION_USERS).document(getUser().getUid()).collection(COLLECTION_STATS).whereEqualTo(FIELD_TOPIC_PLAYED, true);
    }

    private void getTopics(final TopicsListener listener)
    {
        mDatabase.collection(COLLECTION_TOPICS).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>()
        {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots)
            {
                if (!queryDocumentSnapshots.isEmpty() && queryDocumentSnapshots != null)
                {
                    List<String> topicList = new ArrayList<>();
                    for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                    {
                        topicList.add(snapshot.getString("name"));
                    }

                    listener.get(topicList);
                }
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                listener.get(null);
            }
        });
    }

    public DocumentReference getProgressStatsHighScores(String id)
    {
        return mDatabase.collection(COLLECTION_USERS).document(id).collection(COLLECTION_STATS).document(DOCUMENT_PROGRESS);
    }

    private List<String> getProviderData()
    {
        ArrayList<String> providerList = new ArrayList<>();

        for (UserInfo data : getUser().getProviderData())
        {
            providerList.add(data.getProviderId());
        }

        return providerList;
    }
}