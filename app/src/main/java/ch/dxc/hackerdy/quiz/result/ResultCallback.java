package ch.dxc.hackerdy.quiz.result;

import java.util.Map;

public interface ResultCallback
{
    void onFailure(String msg);

    interface ProgressStatsCallback
    {
        void onLevelUp(long level);
    }

    interface PerformanceStatsCallback
    {
        void getPersonalBest(Map<String, Object> personalBestMap);
    }
}