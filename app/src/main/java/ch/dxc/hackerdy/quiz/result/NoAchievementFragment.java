package ch.dxc.hackerdy.quiz.result;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentNoAchievementBinding;

public class NoAchievementFragment extends Fragment
{
    private NoAchievementViewModel mViewModel;

    public static NoAchievementFragment newInstance()
    {
        NoAchievementFragment fragment = new NoAchievementFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
        {
            // TODO
        }

        mViewModel = ViewModelProviders.of(this).get(NoAchievementViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final FragmentNoAchievementBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_achievement, container, false);

        mViewModel.getDisplayName().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String displayName)
            {
                binding.textNoAchievements.setText(String.format("%s, %s! %s", getString(R.string.text_good_round), displayName, getString(R.string.text_no_achievements)));
            }
        });

        return binding.getRoot();
    }
}