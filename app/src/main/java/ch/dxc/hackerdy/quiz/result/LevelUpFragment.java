package ch.dxc.hackerdy.quiz.result;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentLevelUpBinding;

public class LevelUpFragment extends Fragment
{
    private static final String ARG_PARAM_1 = "nextLevel";

    private long nextLevel;

    public static LevelUpFragment newInstance(long nextLevel)
    {
        LevelUpFragment fragment = new LevelUpFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM_1, nextLevel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
        {
            nextLevel = getArguments().getLong(ARG_PARAM_1);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        FragmentLevelUpBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_level_up, container, false);

        binding.textNewLevel.setText(String.format("%s %s!", getString(R.string.text_you_are_now_level), String.valueOf(nextLevel)));

        //  Makes the text blink
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        anim.setStartOffset(0);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        binding.textLevelUp.startAnimation(anim);

        return binding.getRoot();
    }
}