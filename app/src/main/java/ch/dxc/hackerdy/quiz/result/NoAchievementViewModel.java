package ch.dxc.hackerdy.quiz.result;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class NoAchievementViewModel extends ViewModel
{
    private MutableLiveData<String> mDisplayName = new MutableLiveData<>();

    private FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<String> getDisplayName()
    {
        mDisplayName.setValue(mRepository.getUser().getDisplayName());

        return mDisplayName;
    }
}