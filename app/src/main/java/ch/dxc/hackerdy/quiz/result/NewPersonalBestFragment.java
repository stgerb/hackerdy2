package ch.dxc.hackerdy.quiz.result;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentNewPersonalBestBinding;

public class NewPersonalBestFragment extends Fragment
{
    private static final String ARG_APARM_1 = "score";
    private static final String ARG_PARAM_2 = "time";

    private NewPersonalBestViewModel mViewModel;
    private long mScore;
    private String mTime;

    public static NewPersonalBestFragment getInstance(long score, String time)
    {
        NewPersonalBestFragment fragment = new NewPersonalBestFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_APARM_1, score);
        args.putString(ARG_PARAM_2, time);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
        {
            mScore = getArguments().getLong(ARG_APARM_1);
            mTime = getArguments().getString(ARG_PARAM_2);
        }

        mViewModel = ViewModelProviders.of(this).get(NewPersonalBestViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final FragmentNewPersonalBestBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_personal_best, container, false);

        mViewModel.getDisplayName().observe(this, new Observer<String>()
        {
            @Override
            public void onChanged(String displayName)
            {
                binding.textNewPersonalBest.setText(String.format("%s %s %s %s %s. %s, %s!", getString(R.string.text_you_scored), String.valueOf(mScore), getString(R.string.text_points), getString(R.string.text_in), mTime, getString(R.string.text_amazing), displayName));

                //  Makes the text blink
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500);
                anim.setStartOffset(0);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                binding.textTitleNewPersonalBest.startAnimation(anim);
            }
        });

        return binding.getRoot();
    }
}
