package ch.dxc.hackerdy.quiz.question;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class QuestionLiveData extends LiveData<Question> implements EventListener<QuerySnapshot>
{
    private Query mQuery;
    private ListenerRegistration mListener;

    public QuestionLiveData(Query query)
    {
        mQuery = query;
    }

    @Override
    protected void onActive()
    {
        mListener = mQuery.addSnapshotListener(this);
    }

    @Override
    protected void onInactive()
    {
        mListener.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot querySnapshot, @Nullable FirebaseFirestoreException e)
    {
        if (!querySnapshot.isEmpty() && querySnapshot != null)
        {
            int size = querySnapshot.size();
            int index = new Random().nextInt((size));

            DocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(index);
            String topic = documentSnapshot.getString("topic");
            String qst = documentSnapshot.getString("question");
            List<String> answerList = (List<String>) documentSnapshot.get("answers");
            String solution = documentSnapshot.getString("solution");
            long points = documentSnapshot.getLong("points");
            String reason = documentSnapshot.getString("reason");

            setValue(new Question(topic, qst, answerList, solution, points, reason));
        }
        else if (e != null)
        {
            // TODO Error handling
        }
    }
}