package ch.dxc.hackerdy.quiz;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnticipateInterpolator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;
import androidx.transition.ChangeBounds;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityQuizBinding;
import ch.dxc.hackerdy.quiz.question.QuestionActivity;
import ch.dxc.hackerdy.quiz.result.ResultActivity;
import ch.dxc.hackerdy.services.GameTimeService;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final int RC_QUESTION = 1;
    private static final int NUMBER_OF_QUESTIONS_PER_TOPIC = 3;
    private static final int TOTAL_NUMBER_OF_QUESTIONS = 9;

    private ActivityQuizBinding mBinding;
    private GameTimeService mService;
    private int mCounter = 3;
    private int mTopic1Counter = 0;
    private int mTopic2Counter = 0;
    private int mTopic3Counter = 0;
    private int mMultiplier = 0;
    private int mTopic1AnsweredCorrectCounter;
    private int mTopic2AnsweredCorrectCounter;
    private int mTopic3AnsweredCorrectCounter;
    private boolean mPressed = false;
    private boolean mJokerActive = false;
    private boolean mBound = false;
    private MaterialButton mQuizButton;
    private MaterialButton mJokerButton;
    private ArrayList<MaterialButton> mQuizButtonList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quiz);

        mBinding.buttonSkip.setOnClickListener(this);
        mBinding.button100Topic1.setOnClickListener(this);
        mBinding.button100Topic2.setOnClickListener(this);
        mBinding.button100Topic3.setOnClickListener(this);
        mBinding.button200Topic1.setOnClickListener(this);
        mBinding.button200Topic2.setOnClickListener(this);
        mBinding.button200Topic3.setOnClickListener(this);
        mBinding.button300Topic1.setOnClickListener(this);
        mBinding.button300Topic2.setOnClickListener(this);
        mBinding.button300Topic3.setOnClickListener(this);
        mBinding.buttonDoubleUp.setOnClickListener(this);
        mBinding.buttonQuadruple.setOnClickListener(this);
        mBinding.buttonSextuple.setOnClickListener(this);

        mBinding.imageInfo.setOnClickListener(this);

        mQuizButtonList.add(mBinding.button100Topic1);
        mQuizButtonList.add(mBinding.button100Topic2);
        mQuizButtonList.add(mBinding.button100Topic3);
        mQuizButtonList.add(mBinding.button200Topic1);
        mQuizButtonList.add(mBinding.button200Topic2);
        mQuizButtonList.add(mBinding.button200Topic3);
        mQuizButtonList.add(mBinding.button300Topic1);
        mQuizButtonList.add(mBinding.button300Topic2);
        mQuizButtonList.add(mBinding.button300Topic3);

        startCountDown();

        setHeightOfQuizButtons();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (mBound)
        {
            mService.stopTimer();

            unbindService(mServiceConn);
            mBound = false;
        }
    }

    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this).setTitle(R.string.title_leave_quiz).setMessage(R.string.text_leave_quiz).setPositiveButton(R.string.button_leave, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                QuizActivity.super.onBackPressed();
            }
        }).setNegativeButton(R.string.button_stay, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private ServiceConnection mServiceConn = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            GameTimeService.LocalBinder binder = (GameTimeService.LocalBinder) iBinder;
            mService = binder.getService();

            mService.startTimer(mBinding.textGameTime);

            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            mBound = false;
        }
    };

    private void setHeightOfTopicButtons()
    {
        mBinding.activityQuiz.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                mBinding.activityQuiz.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                List<Integer> list = Arrays.asList(mBinding.buttonTopic1.getHeight(), mBinding.buttonTopic2.getHeight(), mBinding.buttonTopic3.getHeight());
                int max = Collections.max(list);
                mBinding.buttonTopic1.setHeight(max);
                mBinding.buttonTopic2.setHeight(max);
                mBinding.buttonTopic3.setHeight(max);
            }
        });
    }

    private void setHeightOfQuizButtons()
    {
        mBinding.activityQuiz.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                mBinding.activityQuiz.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                mBinding.button100Topic1.setHeight(mBinding.button100Topic1.getWidth());
                mBinding.button100Topic2.setHeight(mBinding.button100Topic2.getWidth());
                mBinding.button100Topic3.setHeight(mBinding.button100Topic3.getWidth());
                mBinding.button200Topic1.setHeight(mBinding.button200Topic1.getWidth());
                mBinding.button200Topic2.setHeight(mBinding.button200Topic2.getWidth());
                mBinding.button200Topic3.setHeight(mBinding.button200Topic3.getWidth());
                mBinding.button300Topic1.setHeight(mBinding.button300Topic1.getWidth());
                mBinding.button300Topic2.setHeight(mBinding.button300Topic2.getWidth());
                mBinding.button300Topic3.setHeight(mBinding.button300Topic3.getWidth());

                mBinding.buttonDoubleUp.setHeight(mBinding.buttonDoubleUp.getWidth());
                mBinding.buttonQuadruple.setHeight(mBinding.buttonQuadruple.getWidth());
                mBinding.buttonSextuple.setHeight(mBinding.buttonSextuple.getWidth());
            }
        });
    }

    // TODO on Go, set visibility of text "Quiz starts in" to View.Gone
    private void startCountDown()
    {
        new CountDownTimer(5000, 1000)
        {
            @Override
            public void onTick(long l)
            {
                if (mCounter == 0)
                {
                    mBinding.textCountdownCounter.setText(getString(R.string.text_go));
                }
                else
                {
                    mBinding.textCountdownCounter.setText(String.valueOf(mCounter));
                    mCounter--;
                }
            }

            @Override
            public void onFinish()
            {
                executeLayoutTransition();
            }
        }.start();
    }

    private void executeLayoutTransition()
    {
        mBinding.textQuizStartsIn.setVisibility(View.GONE);
        mBinding.textCountdownCounter.setVisibility(View.GONE);
        mBinding.buttonSkip.setVisibility(View.GONE);

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(mBinding.activityQuiz);
        constraintSet.clone(QuizActivity.this, R.layout.activity_quiz_detail);
        Transition transition = new ChangeBounds();
        transition.setInterpolator(new AnticipateInterpolator(1.0f));
        transition.setDuration(1000);
        TransitionManager.beginDelayedTransition(mBinding.activityQuiz, transition);
        constraintSet.applyTo(mBinding.activityQuiz);

        Intent intent = getIntent();
        ArrayList<String> topicList = intent.getStringArrayListExtra("topics");

        mBinding.buttonTopic1.setText(topicList.get(0));
        mBinding.buttonTopic2.setText(topicList.get(1));
        mBinding.buttonTopic3.setText(topicList.get(2));

        QuizButtonMetadata button100Topic1 = new QuizButtonMetadata(topicList.get(0), 100);
        QuizButtonMetadata button100Topic2 = new QuizButtonMetadata(topicList.get(1), 100);
        QuizButtonMetadata button100Topic3 = new QuizButtonMetadata(topicList.get(2), 100);
        QuizButtonMetadata button200Topic1 = new QuizButtonMetadata(topicList.get(0), 200);
        QuizButtonMetadata button200Topic2 = new QuizButtonMetadata(topicList.get(1), 200);
        QuizButtonMetadata button200Topic3 = new QuizButtonMetadata(topicList.get(2), 200);
        QuizButtonMetadata button300Topic1 = new QuizButtonMetadata(topicList.get(0), 300);
        QuizButtonMetadata button300Topic2 = new QuizButtonMetadata(topicList.get(1), 300);
        QuizButtonMetadata button300Topic3 = new QuizButtonMetadata(topicList.get(2), 300);

        mBinding.button100Topic1.setTag(button100Topic1);
        mBinding.button100Topic2.setTag(button100Topic2);
        mBinding.button100Topic3.setTag(button100Topic3);
        mBinding.button200Topic1.setTag(button200Topic1);
        mBinding.button200Topic2.setTag(button200Topic2);
        mBinding.button200Topic3.setTag(button200Topic3);
        mBinding.button300Topic1.setTag(button300Topic1);
        mBinding.button300Topic2.setTag(button300Topic2);
        mBinding.button300Topic3.setTag(button300Topic3);

        bindService(new Intent(this, GameTimeService.class), mServiceConn, BIND_AUTO_CREATE);

        setHeightOfTopicButtons();
    }

    private void startQuestionActivity(View view)
    {
        mQuizButton = (MaterialButton) view;

        QuizButtonMetadata metadata = (QuizButtonMetadata) mQuizButton.getTag();

        Intent data = new Intent(this, QuestionActivity.class);
        data.putExtra("points", metadata.getPoints());
        data.putExtra("topic", metadata.getTopic());
        data.putExtra("multiplier", mMultiplier);

        startActivityForResult(data, RC_QUESTION);
    }

    private void showTutorialAlertDialog()
    {
        new AlertDialog.Builder(this).setTitle(R.string.title_tutorial).setMessage(R.string.text_tutorial_quiz).setPositiveButton(R.string.button_hide, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
                mService.restartTimer();
            }
        }).create().show();
    }

    // TODO: Fix Bug, when first Quiz Question gets answered to fast,  Quiz Button will turn invisible and back to visible again
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_QUESTION)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                mQuizButtonList.remove(mQuizButton);
                QuizButtonMetadata metadata = (QuizButtonMetadata) mQuizButton.getTag();

                boolean answeredCorrectly = data.getExtras().getBoolean("correct");

                if (metadata.getTopic().equals(mBinding.buttonTopic1.getText().toString()))
                {
                    mTopic1Counter++;

                    if (answeredCorrectly)
                    {
                        mTopic1AnsweredCorrectCounter++;
                    }

                    if (mTopic1Counter == 3)
                    {
                        mBinding.buttonTopic1.setVisibility(View.INVISIBLE);
                    }
                }
                else if (metadata.getTopic().equals(mBinding.buttonTopic2.getText().toString()))
                {
                    mTopic2Counter++;

                    if (answeredCorrectly)
                    {
                        mTopic2AnsweredCorrectCounter++;
                    }

                    if (mTopic2Counter == 3)
                    {
                        mBinding.buttonTopic2.setVisibility(View.INVISIBLE);
                    }
                }
                else
                {
                    mTopic3Counter++;

                    if (answeredCorrectly)
                    {
                        mTopic3AnsweredCorrectCounter++;
                    }

                    if (mTopic3Counter == 3)
                    {
                        mBinding.buttonTopic3.setVisibility(View.INVISIBLE);
                    }
                }

                long score = Long.valueOf(mBinding.textScore.getText().toString().substring(7));
                long newScore = score + data.getExtras().getLong("score");

                if (newScore <= 0)
                {
                    mBinding.textScore.setText(R.string.text_init_score);
                }
                else
                {
                    mBinding.textScore.setText(String.format("Score: %s", newScore));
                }

                if (mJokerActive)
                {
                    mJokerActive = false;
                    mPressed = false;
                    mMultiplier = 0;

                    mJokerButton.getBackground().setAlpha(97);
                    mJokerButton.setEnabled(false);

                    mBinding.buttonDoubleUp.setVisibility(View.VISIBLE);
                    mBinding.buttonQuadruple.setVisibility(View.VISIBLE);
                    mBinding.buttonSextuple.setVisibility(View.VISIBLE);
                }

                mQuizButton.setVisibility(View.INVISIBLE);

                if (mQuizButtonList.size() == 0)
                {
                    int topic1AnsweredWrongCount = NUMBER_OF_QUESTIONS_PER_TOPIC - mTopic1AnsweredCorrectCounter;
                    int topic2AnsweredWrongCount = NUMBER_OF_QUESTIONS_PER_TOPIC - mTopic2AnsweredCorrectCounter;
                    int topic3AnsweredWrongCount = NUMBER_OF_QUESTIONS_PER_TOPIC - mTopic3AnsweredCorrectCounter;

                    int totalAnsweredCorrectCount = mTopic1AnsweredCorrectCounter + mTopic2AnsweredCorrectCounter + mTopic3AnsweredCorrectCounter;
                    int totalAnsweredWrongCount = TOTAL_NUMBER_OF_QUESTIONS - totalAnsweredCorrectCount;

                    Intent result = new Intent(QuizActivity.this, ResultActivity.class);
                    result.putExtra("score", Long.valueOf(mBinding.textScore.getText().toString().substring(7)));
                    result.putExtra("total_answered_correct_count", totalAnsweredCorrectCount);
                    result.putExtra("total_answered_wrong_count", totalAnsweredWrongCount);
                    result.putExtra("topic1_answered_correct_count", mTopic1AnsweredCorrectCounter);
                    result.putExtra("topic2_answered_correct_count", mTopic2AnsweredCorrectCounter);
                    result.putExtra("topic3_answered_correct_count", mTopic3AnsweredCorrectCounter);
                    result.putExtra("topic1_answered_wrong_count", topic1AnsweredWrongCount);
                    result.putExtra("topic2_answered_wrong_count", topic2AnsweredWrongCount);
                    result.putExtra("topic3_answered_wrong_count", topic3AnsweredWrongCount);
                    result.putExtra("time", mBinding.textGameTime.getText().toString());
                    result.putExtra("seconds", mService.getSeconds());
                    result.putExtra("minutes", mService.getMinutes());

                    ArrayList<String> topicList = new ArrayList<>();
                    topicList.add(mBinding.buttonTopic1.getText().toString());
                    topicList.add(mBinding.buttonTopic2.getText().toString());
                    topicList.add(mBinding.buttonTopic3.getText().toString());
                    result.putStringArrayListExtra("played_topics", topicList);

                    startActivity(result);

                    finish();
                }
            }
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.image_info:
                mService.stopTimer();
                showTutorialAlertDialog();
                break;
            case R.id.button_skip:
                executeLayoutTransition();
                break;
            case R.id.button_100_topic_1:
                startQuestionActivity(view);
                break;
            case R.id.button_100_topic_2:
                startQuestionActivity(view);
                break;
            case R.id.button_100_topic_3:
                startQuestionActivity(view);
                break;
            case R.id.button_200_topic_1:
                startQuestionActivity(view);
                break;
            case R.id.button_200_topic_2:
                startQuestionActivity(view);
                break;
            case R.id.button_200_topic_3:
                startQuestionActivity(view);
                break;
            case R.id.button_300_topic_1:
                startQuestionActivity(view);
                break;
            case R.id.button_300_topic_2:
                startQuestionActivity(view);
                break;
            case R.id.button_300_topic_3:
                startQuestionActivity(view);
                break;
            case R.id.button_double_up:
                mJokerButton = (MaterialButton) view;

                if (!mPressed)
                {
                    mBinding.buttonQuadruple.setVisibility(View.GONE);
                    mBinding.buttonSextuple.setVisibility(View.GONE);
                    mPressed = true;
                    mJokerActive = true;
                    mMultiplier = 2;
                    break;
                }
                else
                {
                    mBinding.buttonQuadruple.setVisibility(View.VISIBLE);
                    mBinding.buttonSextuple.setVisibility(View.VISIBLE);
                    mPressed = false;
                    mJokerActive = false;
                    mMultiplier = 0;
                    break;
                }
            case R.id.button_quadruple:
                mJokerButton = (MaterialButton) view;

                if (!mPressed)
                {
                    mBinding.buttonDoubleUp.setVisibility(View.GONE);
                    mBinding.buttonSextuple.setVisibility(View.GONE);
                    mPressed = true;
                    mJokerActive = true;
                    mMultiplier = 4;
                    break;
                }
                else
                {
                    mBinding.buttonDoubleUp.setVisibility(View.VISIBLE);
                    mBinding.buttonSextuple.setVisibility(View.VISIBLE);
                    mPressed = false;
                    mJokerActive = false;
                    mMultiplier = 0;
                    break;
                }

            case R.id.button_sextuple:
                mJokerButton = (MaterialButton) view;

                if (!mPressed)
                {
                    mBinding.buttonDoubleUp.setVisibility(View.GONE);
                    mBinding.buttonQuadruple.setVisibility(View.GONE);
                    mPressed = true;
                    mJokerActive = true;
                    mMultiplier = 6;
                    break;
                }
                else
                {
                    mBinding.buttonDoubleUp.setVisibility(View.VISIBLE);
                    mBinding.buttonQuadruple.setVisibility(View.VISIBLE);
                    mPressed = false;
                    mJokerActive = false;
                    mMultiplier = 0;
                    break;
                }
        }
    }
}