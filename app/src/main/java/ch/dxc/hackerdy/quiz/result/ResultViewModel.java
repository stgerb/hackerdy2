package ch.dxc.hackerdy.quiz.result;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Map;

import ch.dxc.hackerdy.repositories.FirebaseRepository;
import ch.dxc.hackerdy.repositories.PerformanceListener;

public class ResultViewModel extends ViewModel implements ResultCallback, ResultCallback.ProgressStatsCallback, ResultCallback.PerformanceStatsCallback, PerformanceListener
{
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();
    private MutableLiveData<Long> mLevelUp = new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<Map<String, Object>> mPersonalBest = new MutableLiveData<>();
    private MutableLiveData<Map<String, Object>> mNewPersonalBest = new MutableLiveData<>();
    private int mUpdateCount = 0;
    private boolean mUp = false;
    private boolean mPersonalBestUpdate = false;

    private FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<Map<String, Object>> getProgressStats()
    {
        setLoading(true);

        return mRepository.getProgressStats(mRepository.getUser().getUid());
    }

    public LiveData<Map<String, Long>> getCurrentLevelData(long level)
    {
        return mRepository.getLevelData(level);
    }

    public LiveData<Map<String, Long>> getNextLevelData(long level)
    {
        return mRepository.getLevelData(level);
    }

    public void updateStats(long score,
                            List<String> playedTopicsList,
                            Map<String, Integer> answeredCorrectWrongStats,
                            Map<String, Integer> topic1Stats,
                            Map<String, Integer> topic2Stats,
                            Map<String, Integer> topic3Stats,
                            String time)
    {
        mRepository.updateStats(score,
                playedTopicsList,
                answeredCorrectWrongStats,
                topic1Stats,
                topic2Stats,
                topic3Stats,
                time,
                this);
    }

    public void updateLevel(long level)
    {
        mUp = true;

        incUpdateCount();

        mRepository.updateLevel(level, this, this);
    }

    public LiveData<Map<String, Object>> getPersonalBest()
    {
        mRepository.getPerformanceData(this);

        return mPersonalBest;
    }

    public void updatePersonalBest(long score, String time)
    {
        mPersonalBestUpdate = true;

        mRepository.updatePersonalBest(score, time, this, this);
    }

    public boolean isNewPersonalBest()
    {
        return mPersonalBestUpdate;
    }

    public void setLevelUp(Long level)
    {
        mLevelUp.setValue(level);
    }

    public LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public void setLoading(Boolean bool)
    {
        mLoading.setValue(bool);
    }

    public LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    public LiveData<Long> getLevelUp()
    {
        return mLevelUp;
    }

    public boolean isLevelUp()
    {
        return mUp;
    }

    public void incUpdateCount()
    {
        mUpdateCount++;
    }

    public int getUpdateCount()
    {
        return mUpdateCount;
    }

    public LiveData<Map<String, Object>> getNewPersonalBest()
    {
        return mNewPersonalBest;
    }

    @Override
    public void onFailure(String msg)
    {
        mRepositoryError.setValue(msg);
    }

    @Override
    public void onLevelUp(long level)
    {
        setLevelUp(level);
    }

    @Override
    public void getPerformanceData(Map<String, Object> data)
    {
        mPersonalBest.setValue((Map<String, Object>) data.get("personal_best"));
    }

    @Override
    public void getPersonalBest(Map<String, Object> personalBestMap)
    {
        mNewPersonalBest.setValue(personalBestMap);
    }
}