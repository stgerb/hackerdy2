package ch.dxc.hackerdy.quiz.result;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.Map;

import ch.dxc.hackerdy.repositories.PerformanceListener;

public interface ResultRepository
{
    void updateStats(long score,
                     List<String> playedTopicsList,
                     Map<String, Integer> answeredCorrectWrongStats,
                     Map<String, Integer> topic1Stats,
                     Map<String, Integer> topic2Stats,
                     Map<String, Integer> topic3Stats,
                     String neededTime,
                     ResultCallback repo);

    LiveData<Map<String, Object>> getProgressStats(String id);

    LiveData<Map<String, Long>> getLevelData(long level);

    void resetPlayedTopics(ResultCallback repo);

    void getPerformanceData(PerformanceListener callback);

    void updateLevel(long level,
                     ResultCallback repo,
                     ResultCallback.ProgressStatsCallback update);

    void updatePersonalBest(long score, String time, ResultCallback.PerformanceStatsCallback performance, ResultCallback repo);
}