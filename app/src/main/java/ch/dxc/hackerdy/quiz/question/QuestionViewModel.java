package ch.dxc.hackerdy.quiz.question;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class QuestionViewModel extends ViewModel
{
    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<Question> getQuestion(String topic, long points)
    {
        mLoading.setValue(true);

        return mRepository.getQuestion(topic, points);
    }

    LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    public void setLoading(Boolean bool)
    {
        mLoading.setValue(bool);
    }
}