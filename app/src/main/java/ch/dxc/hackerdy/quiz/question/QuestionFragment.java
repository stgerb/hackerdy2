package ch.dxc.hackerdy.quiz.question;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.button.MaterialButton;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentQuestionBinding;
import ch.dxc.hackerdy.util.ImageUtil;

public class QuestionFragment extends Fragment implements View.OnClickListener
{
    private long mPoints;
    private QuestionViewModel mViewModel;
    private CountDownTimer mTimer;
    private int mSeconds = 30;
    private int mMultiplier;
    private String mTopic;
    private MaterialButton mButtonA;
    private MaterialButton mButtonB;
    private MaterialButton mButtonC;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);

        Intent data = getActivity().getIntent();
        mTopic = data.getStringExtra("topic");
        mPoints = data.getLongExtra("points", 0);
        mMultiplier = data.getIntExtra("multiplier", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final FragmentQuestionBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_question, container, false);

        binding.buttonAnswerA.setOnClickListener(this);
        binding.buttonAnswerB.setOnClickListener(this);
        binding.buttonAnswerC.setOnClickListener(this);

        mViewModel.getQuestion(mTopic, mPoints).observe(this, new Observer<Question>()
        {
            @Override
            public void onChanged(Question question)
            {
                mViewModel.setLoading(false);

                AnswerButtonMetadata metadataA = new AnswerButtonMetadata();
                AnswerButtonMetadata metadataB = new AnswerButtonMetadata();
                AnswerButtonMetadata metadataC = new AnswerButtonMetadata();

                if (question.getAnswerList().get(0).equals(question.getSolution()))
                {
                    metadataA.setCorrect(true);

                    metadataB.setCorrect(false);
                    metadataB.setSolution(question.getAnswerList().get(0));
                    metadataB.setReason(question.getReason());
                    metadataB.setImage(getDrawableLeft(binding.buttonAnswerA));

                    metadataC.setCorrect(false);
                    metadataC.setSolution(question.getAnswerList().get(0));
                    metadataC.setReason(question.getReason());
                    metadataC.setImage(getDrawableLeft(binding.buttonAnswerA));
                }
                else if (question.getAnswerList().get(1).equals(question.getSolution()))
                {
                    metadataB.setCorrect(true);

                    metadataA.setCorrect(false);
                    metadataA.setSolution(question.getAnswerList().get(1));
                    metadataA.setReason(question.getReason());
                    metadataA.setImage(getDrawableLeft(binding.buttonAnswerB));

                    metadataC.setCorrect(false);
                    metadataC.setSolution(question.getAnswerList().get(1));
                    metadataC.setReason(question.getReason());
                    metadataC.setImage(getDrawableLeft(binding.buttonAnswerB));
                }
                else
                {
                    metadataC.setCorrect(true);

                    metadataA.setCorrect(false);
                    metadataA.setSolution(question.getAnswerList().get(2));
                    metadataA.setReason(question.getReason());
                    metadataA.setImage(getDrawableLeft(binding.buttonAnswerC));

                    metadataB.setCorrect(false);
                    metadataB.setSolution(question.getAnswerList().get(2));
                    metadataB.setReason(question.getReason());
                    metadataB.setImage(getDrawableLeft(binding.buttonAnswerC));
                }

                binding.textQuestion.setText(question.getQuestion());
                binding.buttonAnswerA.setText(question.getAnswerList().get(0));
                binding.buttonAnswerB.setText(question.getAnswerList().get(1));
                binding.buttonAnswerC.setText(question.getAnswerList().get(2));

                binding.buttonAnswerA.setTag(metadataA);
                binding.buttonAnswerB.setTag(metadataB);
                binding.buttonAnswerC.setTag(metadataC);

                mButtonA = binding.buttonAnswerA;
                mButtonB = binding.buttonAnswerB;
                mButtonC = binding.buttonAnswerC;
            }
        });

        mViewModel.isLoading().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    binding.progressbar.setVisibility(View.VISIBLE);
                    binding.textQuestion.setVisibility(View.GONE);
                    binding.textCountDownTimer.setVisibility(View.GONE);
                    binding.buttonAnswerA.setVisibility(View.GONE);
                    binding.buttonAnswerB.setVisibility(View.GONE);
                    binding.buttonAnswerC.setVisibility(View.GONE);
                }
                else
                {
                    binding.progressbar.setVisibility(View.GONE);
                    binding.textQuestion.setVisibility(View.VISIBLE);
                    binding.textCountDownTimer.setVisibility(View.VISIBLE);
                    binding.buttonAnswerA.setVisibility(View.VISIBLE);
                    binding.buttonAnswerB.setVisibility(View.VISIBLE);
                    binding.buttonAnswerC.setVisibility(View.VISIBLE);
                }
            }
        });

        mTimer = new CountDownTimer(32000, 1000)
        {
            @Override
            public void onTick(long l)
            {
                if (mSeconds >= 10)
                {
                    binding.textCountDownTimer.setText(String.format("00:%s", String.valueOf(mSeconds)));
                }
                else
                {
                    binding.textCountDownTimer.setText(String.format("00:0%s", String.valueOf(mSeconds)));
                }

                if (mSeconds == 5)
                {
                    binding.textCountDownTimer.setTextColor(getResources().getColor(R.color.colorYellow));
                    Drawable drawable = binding.textCountDownTimer.getCompoundDrawables()[0];
                    drawable.setColorFilter(getResources().getColor(R.color.colorYellow), PorterDuff.Mode.SRC_IN);

//                    Makes the text blink
                    Animation anim = new AlphaAnimation(0.0f, 1.0f);
                    anim.setDuration(500);
                    anim.setStartOffset(0);
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    binding.textCountDownTimer.startAnimation(anim);
                }

                mSeconds--;
            }

            @Override
            public void onFinish()
            {
                startWrongAnswerFragmentWithCorrectAnswer(getString(R.string.time_is_up));
            }
        }.start();

        return binding.getRoot();
    }

    private void startWrongAnswerFragmentWithCorrectAnswer(String title)
    {
        AnswerButtonMetadata metadataA = (AnswerButtonMetadata) mButtonA.getTag();
        AnswerButtonMetadata metadataB = (AnswerButtonMetadata) mButtonB.getTag();
        AnswerButtonMetadata metadataC = (AnswerButtonMetadata) mButtonC.getTag();

        if (!metadataA.isCorrect())
        {
            startWrongAnswerFragment(title, metadataA.getImage(), metadataA.getSolution(), metadataA.getReason());
        }
        else if (!metadataB.isCorrect())
        {
            startWrongAnswerFragment(title, metadataB.getImage(), metadataB.getSolution(), metadataB.getReason());
        }
        else
        {
            startWrongAnswerFragment(title, metadataC.getImage(), metadataC.getSolution(), metadataC.getReason());
        }
    }

    private byte[] getDrawableLeft(MaterialButton button)
    {
        return new ImageUtil().convertDrawableToByteArray(button.getCompoundDrawables()[0]);
    }

    private void verifyAnswer(View view)
    {
        MaterialButton button = (MaterialButton) view;
        AnswerButtonMetadata metadata = (AnswerButtonMetadata) button.getTag();

        if (metadata.isCorrect())
        {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                    .replace(R.id.fragment_container_question, CorrectAnswerFragment.newInstance(mMultiplier))
                    .addToBackStack(null)
                    .commit();
        }
        else
        {
            startWrongAnswerFragmentWithCorrectAnswer(getString(R.string.text_wrong));
        }
    }

    public void startWrongAnswerFragment(String title, byte[] image, String solution, String reason)
    {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                .replace(R.id.fragment_container_question, WrongAnswerFragment.newInstance(title, image, solution, reason))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_answer_a:
                verifyAnswer(view);
                break;
            case R.id.button_answer_b:
                verifyAnswer(view);
                break;
            case R.id.button_answer_c:
                verifyAnswer(view);
                break;
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();

        mTimer.cancel();
    }
}