package ch.dxc.hackerdy.quiz.question;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.FragmentWrongAnswerBinding;

public class WrongAnswerFragment extends Fragment implements View.OnClickListener
{
    private static final String ARG_PARAM1 = "title";
    private static final String ARG_PARAM2 = "drawable";
    private static final String ARG_PARAM3 = "solution";
    private static final String ARG_PARAM4 = "reason";

    private String mTitle;
    private byte[] mDrawable;
    private String mSolution;
    private String mReason;
    private long mPoints;

    public static WrongAnswerFragment newInstance(String title, byte[] drawable, String solution, String reason)
    {
        WrongAnswerFragment fragment = new WrongAnswerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        args.putByteArray(ARG_PARAM2, drawable);
        args.putString(ARG_PARAM3, solution);
        args.putString(ARG_PARAM4, reason);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent data = getActivity().getIntent();
        mPoints = data.getLongExtra("points", 0) * -1;

        if (getArguments() != null)
        {
            mTitle = getArguments().getString(ARG_PARAM1);
            mDrawable = getArguments().getByteArray(ARG_PARAM2);
            mSolution = getArguments().getString(ARG_PARAM3);
            mReason = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        FragmentWrongAnswerBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wrong_answer, container, false);

        binding.buttonContinue.setOnClickListener(this);

        binding.textTitle.setText(mTitle);
        binding.textPoints.setText(String.format("%s points", String.valueOf(mPoints)));
        binding.inputSolution.setText(mSolution);

        Drawable drawable = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(mDrawable, 0, mDrawable.length));
        binding.inputSolution.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        binding.textReason.setText(mReason);

        return binding.getRoot();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_continue:
                Intent data = new Intent();
                data.putExtra("score", mPoints);
                data.putExtra("correct", false);

                getActivity().setResult(Activity.RESULT_OK, data);

                getActivity().finish();
                break;
        }
    }
}