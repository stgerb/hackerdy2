package ch.dxc.hackerdy.forgotpassword;

public interface ForgotPasswordRepository
{
    void sendPasswordReset(String email, PasswordResetCallback reset);
}