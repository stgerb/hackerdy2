package ch.dxc.hackerdy.forgotpassword;

import android.util.Patterns;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class ForgotPasswordViewModel extends ViewModel implements PasswordResetCallback
{
    public final ObservableField<String> mEmail = new ObservableField<>("");

    private MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    private MutableLiveData<Integer> mEmailError = new MutableLiveData<>();
    private MutableLiveData<Boolean> mPasswordResetSent = new MutableLiveData<>();
    private MutableLiveData<String> mRepositoryError = new MutableLiveData<>();

    private FirebaseRepository mRepository = new FirebaseRepository();

    void sendPasswordReset()
    {
        String email = mEmail.get().trim();

        if (email.isEmpty())
        {
            mEmailError.setValue(R.string.error_empty_email);
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            mEmailError.setValue(R.string.error_email_format);
        }
        else
        {
            mRepository.sendPasswordReset(email, this);
        }
    }

    @Override
    public void onStart()
    {
        mLoading.setValue(true);
    }

    @Override
    public void onSuccess()
    {
        mLoading.setValue(false);
        mPasswordResetSent.setValue(true);
    }

    @Override
    public void onFailure(String msg)
    {
        mLoading.setValue(false);
        mRepositoryError.setValue(msg);
    }

    LiveData<String> getRepositoryError()
    {
        return mRepositoryError;
    }

    LiveData<Boolean> isLoading()
    {
        return mLoading;
    }

    LiveData<Integer> getEmailError()
    {
        return mEmailError;
    }

    LiveData<Boolean> getPasswordResetSent()
    {
        return mPasswordResetSent;
    }


}