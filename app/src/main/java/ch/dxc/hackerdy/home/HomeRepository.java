package ch.dxc.hackerdy.home;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public interface HomeRepository
{
    void signOut(GoogleSignInClient googleClient);
}