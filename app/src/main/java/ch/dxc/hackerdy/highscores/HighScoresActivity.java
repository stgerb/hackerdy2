package ch.dxc.hackerdy.highscores;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityHighScoresBinding;
import ch.dxc.hackerdy.stats.StatsActivity;

public class HighScoresActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, View.OnClickListener, AdapterView.OnItemSelectedListener
{
    private HighScoresViewModel mViewModel;
    private ActivityHighScoresBinding mBinding;

    private HighScoresAdapter mAdapter;
    private boolean mEmailSuffixSelected = false;
    private boolean mUsernameSelected = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_high_scores);

        mViewModel = ViewModelProviders.of(this).get(HighScoresViewModel.class);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding.recyclerviewHighScores.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mBinding.recyclerviewHighScores.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mBinding.recyclerviewHighScores.getContext(), layoutManager.getOrientation());
        mBinding.recyclerviewHighScores.addItemDecoration(dividerItemDecoration);

        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this, R.array.high_scores_search_array, R.layout.spinner_item);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mBinding.spinnerSearchCriteria.setAdapter(arrayAdapter);

        mBinding.buttonRefresh.setOnClickListener(this);
        mBinding.spinnerSearchCriteria.setOnItemSelectedListener(this);

        getHighScores();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finishActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.search_menu);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query)
    {
        // Not in use
        return false;
    }

    @Override
    public boolean onQueryTextChange(String searchText)
    {
        mAdapter.getFilter().filter(searchText);

        return false;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_refresh:
                getHighScores();
        }
    }

    @Override
    public void onBackPressed()
    {
        finishActivity();
    }

    private void getHighScores()
    {
        // TODO: Implement real time updates. Only realtime updates possible atm when the main user document changes, but not the sub collections.
        mViewModel.getHighScores().observe(this, new Observer<List<HighScoresListItem>>()
        {
            @Override
            public void onChanged(List<HighScoresListItem> highScoresList)
            {
                mAdapter = new HighScoresAdapter(highScoresList);
                mBinding.recyclerviewHighScores.setAdapter(mAdapter);
            }
        });
    }

    private void finishActivity()
    {
        Intent stats = new Intent(this, StatsActivity.class);
        stats.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(stats);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
    {
        String item = adapterView.getItemAtPosition(pos).toString();
        if (item.equals(getString(R.string.spinner_username)))
        {
            mUsernameSelected = true;
            mEmailSuffixSelected = false;
        }
        else if (item.equals(getString(R.string.spinner_email_suffix)))
        {
            mEmailSuffixSelected = true;
            mUsernameSelected = false;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {
        // Not in use
    }


    public class HighScoresAdapter extends RecyclerView.Adapter<HighScoresAdapter.HighScoresViewHolder> implements Filterable
    {
        private List<HighScoresListItem> mHighScoreListItemList;
        private List<HighScoresListItem> mHighScoreListItemListFull;

        public class HighScoresViewHolder extends RecyclerView.ViewHolder
        {
            public ImageView profilePicture;
            public TextView textRank;
            public TextView textUsername;
            public TextView textBalance;
            public TextView textLevel;
            public ProgressBar progressBarLoadProfilePicture;

            public HighScoresViewHolder(@NonNull View itemView)
            {
                super(itemView);

                profilePicture = itemView.findViewById(R.id.image_profile_picture);
                textRank = itemView.findViewById(R.id.text_rank);
                textUsername = itemView.findViewById(R.id.text_username);
                textBalance = itemView.findViewById(R.id.text_balance);
                textLevel = itemView.findViewById(R.id.text_level);
                progressBarLoadProfilePicture = itemView.findViewById(R.id.progressbar_on_loading_profile_picture);
            }
        }

        public HighScoresAdapter(List<HighScoresListItem> highScoresListItemList)
        {
            mHighScoreListItemList = highScoresListItemList;
            mHighScoreListItemListFull = new ArrayList<>(highScoresListItemList);
        }

        @NonNull
        @Override
        public HighScoresAdapter.HighScoresViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_high_scores, parent, false);

            return new HighScoresViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final HighScoresViewHolder holder, int position)
        {
            final HighScoresListItem currentListItem = mHighScoreListItemList.get(position);

            Picasso.get().load(currentListItem.getPhotoUrl()).into(holder.profilePicture, new Callback()
            {
                @Override
                public void onSuccess()
                {
                    holder.progressBarLoadProfilePicture.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e)
                {
                    // Not in use
                }
            });

            holder.textRank.setText(String.format("%s.", String.valueOf(currentListItem.getRank())));
            holder.textUsername.setText(currentListItem.getUsername());
            holder.textBalance.setText(String.format("%s %s", String.valueOf(currentListItem.getBalance()), holder.itemView.getContext().getString(R.string.text_points)));
            holder.textLevel.setText(String.format("%s %s", holder.itemView.getContext().getString(R.string.text_level), String.valueOf(currentListItem.getLevel())));

            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    int position = holder.getAdapterPosition();
                    int index = mHighScoreListItemListFull.indexOf(mHighScoreListItemList.get(position));
                    HighScoresListItem item = mHighScoreListItemListFull.get(index);

                    Intent data = new Intent(HighScoresActivity.this, StatsActivity.class);
                    data.putExtra("id", item.getId());
                    startActivity(data);
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return mHighScoreListItemList.size();
        }

        @Override
        public Filter getFilter()
        {
            return highScoresFilter;
        }

        private Filter highScoresFilter = new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence)
            {
                List<HighScoresListItem> filteredList = new ArrayList<>();

                if (charSequence == null || charSequence.length() == 0)
                {
                    filteredList.addAll(mHighScoreListItemListFull);
                }
                else
                {
                    String searchText = charSequence.toString().toLowerCase().trim();
                    for (HighScoresListItem item : mHighScoreListItemListFull)
                    {
                        if (mUsernameSelected)
                        {
                            if (item.getUsername().toLowerCase().contains(searchText))
                            {
                                filteredList.add(item);
                            }
                        }
                        else if (mEmailSuffixSelected)
                        {
                            if (item.getDomain().toLowerCase().contains(searchText))
                            {
                                filteredList.add(item);
                            }
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence searchText, FilterResults filterResults)
            {
                if (searchText.length() == 0)
                {
                    if (!mBinding.buttonRefresh.isShown())
                    {
                        mBinding.buttonRefresh.show();
                    }
                }
                else
                {
                    if (mBinding.buttonRefresh.isShown())
                    {
                        mBinding.buttonRefresh.hide();
                    }
                }

                mHighScoreListItemList.clear();
                mHighScoreListItemList.addAll((List) filterResults.values);

                notifyDataSetChanged();
            }
        };
    }
}