package ch.dxc.hackerdy.highscores;

public class HighScoresListItem
{
    private long mRank;
    private long mLevel;
    private long mBalance;
    private String mPhotoUrl;
    private String mUsername;
    private String mDomain;
    private String mId;

    public HighScoresListItem(String id, String photoUrl, String username, String domain, long balance, long level)
    {
        mLevel = level;
        mPhotoUrl = photoUrl;
        mUsername = username;
        mDomain = domain;
        mBalance = balance;
        mId = id;
    }

    public String getId()
    {
        return mId;
    }

    public long getLevel()
    {
        return mLevel;
    }

    public long getRank()
    {
        return mRank;
    }

    public String getPhotoUrl()
    {
        return mPhotoUrl;
    }

    public String getUsername()
    {
        return mUsername;
    }

    public String getDomain()
    {
        return mDomain;
    }

    public long getBalance()
    {
        return mBalance;
    }

    public void setRank(long rank)
    {
        mRank = rank;
    }
}