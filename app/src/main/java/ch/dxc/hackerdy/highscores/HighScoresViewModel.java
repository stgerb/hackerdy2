package ch.dxc.hackerdy.highscores;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import ch.dxc.hackerdy.repositories.FirebaseRepository;

public class HighScoresViewModel extends ViewModel
{
    private FirebaseRepository mRepository = new FirebaseRepository();

    public LiveData<List<HighScoresListItem>> getHighScores()
    {
        return mRepository.getHighScores();
    }

    public String getUserId()
    {
        return mRepository.getUser().getUid();
    }
}