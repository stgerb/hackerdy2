package ch.dxc.hackerdy.highscores;

import androidx.lifecycle.LiveData;

import java.util.List;

public interface HighScoresRepository
{
    LiveData<List<HighScoresListItem>> getHighScores();
}