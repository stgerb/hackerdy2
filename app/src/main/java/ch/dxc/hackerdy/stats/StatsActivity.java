package ch.dxc.hackerdy.stats;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import ch.dxc.hackerdy.R;
import ch.dxc.hackerdy.databinding.ActivityStatsBinding;
import ch.dxc.hackerdy.highscores.HighScoresActivity;
import ch.dxc.hackerdy.highscores.HighScoresListItem;
import ch.dxc.hackerdy.home.HomeActivity;
import ch.dxc.hackerdy.questionnaire.TopicListItem;

public class StatsActivity extends AppCompatActivity implements View.OnClickListener
{
    private StatsViewModel mViewModel;
    private ActivityStatsBinding mBinding;
    private TextView mTextTopicPerformance;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_stats);

        mViewModel = ViewModelProviders.of(this).get(StatsViewModel.class);

        mBinding.buttonHighScores.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("id"))
        {
            mViewModel.setId(bundle.getString("id"));
        }
        else
        {
            mViewModel.setId(null);
        }

        // TODO: Test dynamic Topic fetching by adding new Topics to DB: New Topic must be added to every user also
        mViewModel.getTopics().observe(this, new Observer<List<TopicListItem>>()
        {
            @Override
            public void onChanged(List<TopicListItem> topicListItems)
            {
                for (int i = 0; i < topicListItems.size(); i++)
                {
                    TextView text0Percent = new TextView(StatsActivity.this);
                    TextView text100Percent = new TextView(StatsActivity.this);
                    TextView textTopicTitle = new TextView(StatsActivity.this);
                    final TextView textTopicPerformance = new TextView(StatsActivity.this);
                    final ProgressBar progressbarTopicPerformance = new ProgressBar(StatsActivity.this, null, android.R.attr.progressBarStyleHorizontal);

                    text0Percent.setText(R.string.text_0_percent);
                    text100Percent.setText(R.string.text_100_percent);
                    textTopicTitle.setText(topicListItems.get(i).getName());

                    mViewModel.getTopicStats(topicListItems.get(i).getName()).observe(StatsActivity.this, new Observer<Map<String, Long>>()
                    {
                        @Override
                        public void onChanged(Map<String, Long> topicStatsMap)
                        {
                            long answeredCorrect = topicStatsMap.get("answered_correct");
                            long answeredWrong = topicStatsMap.get("answered_wrong");

                            long total = answeredCorrect + answeredWrong;
                            double performance;
                            if (total == 0)
                            {
                                performance = 0;
                            }
                            else
                            {
                                performance = ((double) answeredCorrect / total) * 100;
                            }

                            progressbarTopicPerformance.setProgress((int) performance);

                            DecimalFormat df = new DecimalFormat();
                            df.setMaximumFractionDigits(2);
                            textTopicPerformance.setText(String.format("%s%% (%s:%s) # Plays: %s", df.format(performance), String.valueOf(answeredCorrect), String.valueOf(answeredWrong), topicStatsMap.get("play_count")));
                        }
                    });

                    textTopicTitle.setTextAppearance(StatsActivity.this, R.style.TextAppearance_MaterialComponents_Subtitle2);
                    textTopicTitle.setTextColor(getResources().getColor(android.R.color.white));

                    text0Percent.setTextAppearance(StatsActivity.this, R.style.TextAppearance_MaterialComponents_Caption);
                    text0Percent.setTextColor(getResources().getColor(android.R.color.white));

                    text100Percent.setTextAppearance(StatsActivity.this, R.style.TextAppearance_MaterialComponents_Caption);
                    text100Percent.setTextColor(getResources().getColor(android.R.color.white));

                    textTopicPerformance.setTextAppearance(StatsActivity.this, R.style.TextAppearance_MaterialComponents_Caption);
                    textTopicPerformance.setTextColor(getResources().getColor(android.R.color.white));

                    text0Percent.setId(View.generateViewId());
                    text100Percent.setId(View.generateViewId());
                    textTopicTitle.setId(View.generateViewId());
                    textTopicPerformance.setId(View.generateViewId());
                    progressbarTopicPerformance.setId(View.generateViewId());

                    mBinding.activityStats.addView(textTopicTitle);
                    mBinding.activityStats.addView(progressbarTopicPerformance);
                    mBinding.activityStats.addView(text0Percent);
                    mBinding.activityStats.addView(text100Percent);
                    mBinding.activityStats.addView(textTopicPerformance);

                    ConstraintSet set = new ConstraintSet();
                    set.clone(mBinding.activityStats);

                    set.centerHorizontally(textTopicTitle.getId(), R.id.activity_stats);
                    set.connect(textTopicTitle.getId(), ConstraintSet.START, progressbarTopicPerformance.getId(), ConstraintSet.START, 0);
                    set.connect(textTopicTitle.getId(), ConstraintSet.END, progressbarTopicPerformance.getId(), ConstraintSet.END, 0);

                    if (i == 0) // for the first constraint set
                    {
                        set.connect(textTopicTitle.getId(), ConstraintSet.TOP, R.id.text_topic_stats, ConstraintSet.BOTTOM, 64);
                    }
                    else
                    {
                        set.connect(textTopicTitle.getId(), ConstraintSet.TOP, mTextTopicPerformance.getId(), ConstraintSet.BOTTOM, 96);
                    }

                    set.centerHorizontally(progressbarTopicPerformance.getId(), R.id.activity_stats);
                    set.connect(progressbarTopicPerformance.getId(), ConstraintSet.START, text0Percent.getId(), ConstraintSet.END, 16);
                    set.connect(progressbarTopicPerformance.getId(), ConstraintSet.END, text100Percent.getId(), ConstraintSet.START, 16);
                    set.connect(progressbarTopicPerformance.getId(), ConstraintSet.TOP, textTopicTitle.getId(), ConstraintSet.BOTTOM, 0);
                    set.constrainWidth(progressbarTopicPerformance.getId(), ConstraintSet.MATCH_CONSTRAINT);

                    set.connect(text0Percent.getId(), ConstraintSet.START, mBinding.guidelineLeft.getId(), ConstraintSet.END, 0);
                    set.connect(text0Percent.getId(), ConstraintSet.TOP, progressbarTopicPerformance.getId(), ConstraintSet.TOP, 0);
                    set.connect(text0Percent.getId(), ConstraintSet.BOTTOM, progressbarTopicPerformance.getId(), ConstraintSet.BOTTOM, 0);
                    set.setHorizontalBias(text0Percent.getId(), 0.0f);

                    set.connect(text100Percent.getId(), ConstraintSet.END, mBinding.guidelineRight.getId(), ConstraintSet.START, 0);
                    set.connect(text100Percent.getId(), ConstraintSet.TOP, progressbarTopicPerformance.getId(), ConstraintSet.TOP, 0);
                    set.connect(text100Percent.getId(), ConstraintSet.BOTTOM, progressbarTopicPerformance.getId(), ConstraintSet.BOTTOM, 0);
                    set.setHorizontalBias(text100Percent.getId(), 1.0f);

                    set.connect(textTopicPerformance.getId(), ConstraintSet.TOP, progressbarTopicPerformance.getId(), ConstraintSet.BOTTOM, 0);
                    set.connect(textTopicPerformance.getId(), ConstraintSet.START, progressbarTopicPerformance.getId(), ConstraintSet.START, 0);
                    set.connect(textTopicPerformance.getId(), ConstraintSet.END, progressbarTopicPerformance.getId(), ConstraintSet.END, 0);

                    if (i == topicListItems.size() - 1) // for the last constraint set
                    {
                        set.connect(textTopicPerformance.getId(), ConstraintSet.BOTTOM, mBinding.guidelineBottom.getId(), ConstraintSet.TOP, 0);
                    }

                    set.applyTo(mBinding.activityStats);

                    mTextTopicPerformance = textTopicPerformance;
                }

                mViewModel.setLoadingTopics(false);
            }
        });

        mViewModel.getProgressStats().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> progressStatsMap)
            {
                long currentLevel = (long) progressStatsMap.get("level");
                final long balance = (long) progressStatsMap.get("balance");

                mBinding.textBalance.setText(String.valueOf(progressStatsMap.get("balance")));
                mViewModel.setLoadingBalance(false);

                mBinding.textLevel.setText(String.valueOf(currentLevel));
                mViewModel.setLoadingLevel(false);

                mBinding.textCurrentLevel.setText(String.valueOf(currentLevel));

                currentLevel++;

                mViewModel.getLevelData(currentLevel).observe(StatsActivity.this, new Observer<Map<String, Long>>()
                {
                    @Override
                    public void onChanged(Map<String, Long> nextLevelDataMap)
                    {
                        long nextLevel = nextLevelDataMap.get("level");
                        long nextLevelPoints = nextLevelDataMap.get("points");

                        mBinding.textNextLevel.setText(String.valueOf(nextLevel));
                        mBinding.textProgress.setText(String.format("%s %s %s %s", String.valueOf(balance), getString(R.string.text_of), String.valueOf(nextLevelPoints), getString(R.string.text_points)));

                        double progress = ((double) balance / nextLevelPoints) * 100;

                        mBinding.progressbarProgress.setProgress((int) progress);

                        mViewModel.setLoadingProgress(false);
                    }
                });
            }
        });

        mViewModel.getUserData().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> userDataMap)
            {
                Uri photoUrl = Uri.parse(userDataMap.get("photo").toString());
                final String username = userDataMap.get("username").toString();

                Picasso.get().load(photoUrl).into(mBinding.imageProfilePicture, new Callback()
                {
                    @Override
                    public void onSuccess()
                    {
                        mViewModel.setLoadingProfilePicture(false);
                    }

                    @Override
                    public void onError(Exception e)
                    {
                        // Not in use
                    }
                });

                mBinding.textUsername.setText(username);
                mViewModel.setLoadingUsername(false);

                mViewModel.getRank().observe(StatsActivity.this, new Observer<List<HighScoresListItem>>()
                {
                    @Override
                    public void onChanged(List<HighScoresListItem> highScoresListItemList)
                    {
                        for (HighScoresListItem item : highScoresListItemList)
                        {
                            if (item.getUsername().equals(username))
                            {
                                mBinding.textRank.setText(String.valueOf(item.getRank()));
                                mViewModel.setLoadingRank(false);
                                break;
                            }
                        }
                    }
                });
            }
        });

        mViewModel.getPerformanceStats().observe(this, new Observer<Map<String, Object>>()
        {
            @Override
            public void onChanged(Map<String, Object> performanceStatsMap)
            {
                mBinding.textPlayTime.setText(String.valueOf(performanceStatsMap.get("play_time")));
                mViewModel.setLoadingPlayTime(false);

                if (String.valueOf(performanceStatsMap.get("personal_best")).equals("-"))
                {
                    mBinding.textPersonalBest.setText("-");
                }
                else
                {
                    Map<String, Object> personalBest = (Map<String, Object>) performanceStatsMap.get("personal_best");
                    mBinding.textPersonalBest.setText(String.format("%s p/%s", String.valueOf(personalBest.get("score")), personalBest.get("time")));
                }
                mViewModel.setLoadingPersonalBest(false);

                mBinding.textPlayedQuizzes.setText(String.valueOf(performanceStatsMap.get("played_quizzes")));
                mViewModel.setLoadingPlayedQuizzes(false);

                long answeredCorrect = (long) performanceStatsMap.get("answered_correct");
                long answeredWrong = (long) performanceStatsMap.get("answered_wrong");

                long total = answeredCorrect + answeredWrong;
                double performance;
                if (total == 0)
                {
                    performance = 0;
                }
                else
                {
                    performance = ((double) answeredCorrect / total) * 100;
                }

                mBinding.progressbarPerformance.setProgress((int) performance);

                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(2);
                mBinding.textPerformance.setText(String.format("%s%% (%s:%s)", df.format(performance), String.valueOf(answeredCorrect), String.valueOf(answeredWrong)));

                mViewModel.setLoadingPerformance(false);
            }
        });

        mViewModel.isLoadingRank().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadRank.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadRank.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingPerformance().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadPerformance.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadPerformance.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingProgress().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadProgress.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadProgress.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingBalance().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadBalance.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadBalance.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingLevel().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadLevel.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadLevel.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingPlayTime().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadPlayTime.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadPlayTime.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingPersonalBest().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadPersonalBest.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadPersonalBest.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingPlayedQuizzes().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadPlayedQuizzes.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadPlayedQuizzes.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingProfilePicture().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadProfilePicture.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadProfilePicture.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingUsername().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadUsername.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadUsername.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.isLoadingTopics().observe(this, new Observer<Boolean>()
        {
            @Override
            public void onChanged(Boolean loading)
            {
                if (loading)
                {
                    mBinding.progressbarLoadingTopics.setVisibility(View.VISIBLE);
                }
                else
                {
                    mBinding.progressbarLoadingTopics.setVisibility(View.GONE);
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finishActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_high_scores:
                startActivity(new Intent(this, HighScoresActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        finishActivity();
    }

    public void finishActivity()
    {
        Intent home = new Intent(this, HomeActivity.class);
        home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(home);
    }
}